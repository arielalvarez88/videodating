import { GenderSelection } from 'app/components/genderSelection/GenderSelection'
import renderer from 'react-test-renderer'
import Genders from 'app/enums/Genders'
import React from 'react'
import {findById} from 'frontend/__tests__/TestUtil.js'
jest.mock('Button', () => {

    return 'Button';
});


describe("GenderSelection component tests", () => {


    test("Clicking Male should call setGender function property with the right value", () => {
        const setGender = jest.fn()
        const component = renderer.create(<GenderSelection setGender={setGender} />).toJSON()
        const maleButton = findById(component,'male_btn')
        maleButton.props.onPress()
        expect(setGender).toBeCalledWith(Genders.MALE)
    })

    test("Clicking Female should call setGender function property with the right value", () => {
        const setGender = jest.fn()
        const component = renderer.create(<GenderSelection setGender={setGender} />).toJSON()
        const femaleBtn = findById(component,'female_btn')
        femaleBtn.props.onPress()
        expect(setGender).toBeCalledWith(Genders.FEMALE)
    })


})