
import React from 'react';
import renderer from 'react-test-renderer';
import { VideoReactions } from 'app/components/videoReactions/VideoReactions';
import { findById } from '../../../TestUtil'
import videoReactionsStyle from 'app/components/videoReactions/VideoReactionsStyle'
import VideoRatings from 'app/enums/VideoRatings'

jest.mock('TouchableHighlight', () => {

    return 'TouchableHighlight';
});

describe("VideoReactions component tests", () => {


    test("click an icon should apply selected style", () => {
        const dispatchReactToVideo = jest.fn()
        const profile = {
            id : 'xx',
            principalVideo: {
                uri: '/Ariel/test.mp4'
            }
        }

        const videoReactions = renderer.create(<VideoReactions dispatchReactToVideo={dispatchReactToVideo} uri={profile.principalVideo.uri} userId={profile.id} />)
        let goodBtn = findById(videoReactions.toJSON(), VideoRatings.GOOD)
        goodBtn.props.onPress()
        videoReactions.update(<VideoReactions />)
        goodBtn = findById(videoReactions.toJSON(), VideoRatings.GOOD)
        expect(dispatchReactToVideo).toBeCalledWith({
            userId: profile.id,
            uri: profile.principalVideo.uri,
            rate: VideoRatings.GOOD
        })
        expect(videoReactions.root.instance.state.styles.good).toEqual(videoReactionsStyle.pressedRateIcon)


    })

    test("If profile has rated this video in the past, it should highlight the previosly given rating", () => {

        const reactToVideo = jest.fn()
        const otherProfile = {
            id: 'xx',
            principalVideo: {
                id: 'aa',
                uri: '/Ariel/test.mp4'
            }
        }
        const previousReaction = VideoRatings.COOL
        const myPreviousReactions = {
            [otherProfile.id]: {
                [otherProfile.principalVideo.id]: previousReaction
            }
        }

        const videoReactions = renderer.create(<VideoReactions dispatchReactToVideo={reactToVideo} videoId={otherProfile.principalVideo.id} userId={otherProfile.id} myPreviousReactions={myPreviousReactions} uri={otherProfile.principalVideo.uri}/>)


        
        let buttonOfpreviousReaction = findById(videoReactions.toJSON(), previousReaction)
        
        
        expect(buttonOfpreviousReaction.props.style).toEqual(videoReactionsStyle.pressedRateIcon)
    })

    test("clicking an icon should dispatch request_video_vote with right params", () => {

    })
})