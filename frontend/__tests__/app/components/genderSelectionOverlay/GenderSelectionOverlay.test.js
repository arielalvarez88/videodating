import React from 'react'
import renderer from 'react-test-renderer'
import { GenderSelectionOverlay } from 'app/components/genderSelectionOverlay/GenderSelectionOverlay'

jest.mock('app/components/genderSelection/GenderSelection',()=>{
    return 'GenderSelection'
})
describe("GenderSelectionOverlay component's tests", () => {
   
    test("When isVisible prop is true it renders", () => {
        const isVisible = true
        const tree = renderer.create(<GenderSelectionOverlay isVisible={isVisible} />).toJSON()
        expect(tree).toMatchSnapshot()
    })

    test("When isVisible prop is false it doesn't render", () => {
        const isVisible = false
        const tree = renderer.create(<GenderSelectionOverlay isVisible={isVisible} />).toJSON()
        expect(tree).toMatchSnapshot()
    })

})