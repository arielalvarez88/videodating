//@flow

import backendReducers from 'app/stores/reducers/Backend'
import Actions from '../../../../app/stores/Actions';
import VideoRatings from '../../../../app/enums/VideoRatings';
import type { RateIntent } from '../../../../app/stores/actionCreators/BackendActions';
import Genders from 'app/enums/Genders'

describe("Backend reducers tests", ()=>{

    test("Action type request_matches_success should set fetchingMatches = false and lastSuggestions equal to whatever is sent in the action.matches", ()=>{
        const state = {}
        const matches = {a:1}
        const newState = backendReducers(state,{type:'request_matches_success', matches})
        expect(newState).toMatchSnapshot()

    })

    test(`Action type  ${Actions.REQUEST_VIDEO_RATING_SUCCESS} update video field in the profile`, ()=>{
        const state = {
            profile: {
                reactionsToVideos: {
                    '1' : {
                        'uri1' : VideoRatings.GOOD
                    }
                }
            }
        }
        const rateIntent : RateIntent = {
            uri : 'uri2',
            userId : '2',
            videoId: 'abc',
            rate : VideoRatings.BAD
        }
        const successAction =   {
            type: Actions.REQUEST_VIDEO_RATING_SUCCESS,
            rateIntent: rateIntent
        }
        const newState = backendReducers(state,{type:Actions.REQUEST_VIDEO_RATING_SUCCESS,  rateIntent})
        expect(newState).toMatchSnapshot()

    })

    test(`Action type  ${Actions.REQUEST_SET_GENDER_SUCCESS} updates the gender field in the profile`, ()=>{
        const state = {
            profile: {
                reactionsToVideos: {
                    '1' : {
                        'uri1' : VideoRatings.GOOD
                    }
                }
            }
        }
       
        const successAction =   {
            type: Actions.REQUEST_SET_GENDER_SUCCESS,
            gender: Genders.MALE
        }
        const newState = backendReducers(state, successAction)
        expect(newState).toMatchSnapshot()

    })

    test(`Action type  ${Actions.POP_PROFILE_SUGGESTION} pops from lastSuggestions array`, ()=>{
        const suggestions = [
            {id:'1'},
            {id:'2'}
        ]
        const state = {
            lastSuggestions: suggestions
        }
       
        const popProfileAction =   {
            type: Actions.POP_PROFILE_SUGGESTION
        }
        const newState = backendReducers(state, popProfileAction)
        expect(newState).toMatchSnapshot()

    })
})