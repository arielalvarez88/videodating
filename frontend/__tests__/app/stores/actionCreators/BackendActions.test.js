//@flow

import { getMatches, setGender } from 'app/stores/actionCreators/BackendActions'
import DistanceUnits from 'app/enums/DistanceUnits';
import LocationUtils from 'app/utils/LocationUtils'
import UserTypes from 'app/enums/UserTypes'
import { PermissionsAndroid } from 'react-native';
import { reactToVideo, popProfileSuggestion } from 'app/stores/actionCreators/BackendActions';
import type { RateIntent } from 'app/stores/actionCreators/BackendActions'
import { defaultProfile } from 'app/models/Profile'
import type { Profile } from 'app/models/Profile'
import VideoRatings from 'app/enums/VideoRatings'
import Actions from '../../../../app/stores/Actions'
jest.mock('app/utils/backend/BackendProxy')
import backendProxy from 'app/utils/backend/BackendProxy'
import mockBackendProxy from 'app/utils/backend/__mocks__/BackendProxy.js'
import { Alert } from 'react-native';
import Genders from 'app/enums/Genders'
import type { JestMockT } from "jest"

function _mock(mockFn) {
    return ((mockFn: any): JestMockT);
}
var dispatch = jest.fn()

var mockProfile: Profile = Object.assign({}, defaultProfile, {
    paidLocation: [2, 3],
    userType: UserTypes.NORMAL

})

mockProfile.settings = {
    ...mockProfile.settings,
    maxDistance: 10,
    distanceUnit: DistanceUnits.KM

}


describe("Tests for backend actions", () => {

    beforeEach(() => {
        jest.clearAllMocks()

    })

    describe("getMatches action tests", () => {

        var mockSpy, pemissionsMock, liveLocation = [Math.random(), Math.random()]

        beforeAll(() => {
            mockSpy = jest.spyOn(LocationUtils, 'getLatLong')
            pemissionsMock = jest.spyOn(PermissionsAndroid, 'check')

            pemissionsMock.mockImplementation(() => {
                return new Promise((resolve) => {
                    resolve(true)
                })
            })
            mockSpy.mockImplementation(() => {
                return new Promise((resolve) => {
                    resolve(liveLocation)
                })
            })
        })

        afterAll(() => {
            //jest.restoreAllMocks()
        })


        describe("When a user with userType = UserTypes.Normal calls getMatches, we should detect his location live, but for userType = UserTypes.PREMIUM the location is taken from staticLocation field in profile", () => {

            beforeEach(() => {
                mockProfile.settings.distanceUnit = DistanceUnits.KM

                jest.clearAllMocks()
            })

            test("Non paid users gets matches with live location", async () => {
                await getMatches(mockProfile)(dispatch)
                expect(LocationUtils.getLatLong).toBeCalled()
                expect(backendProxy.getMatches).toBeCalledWith({
                    latLong: liveLocation,
                    radius: mockProfile.settings.maxDistance
                })
            })


            test("For Premium users we should get location from paidLocation field", async () => {
                mockProfile.userType = UserTypes.PREMIUM
                await getMatches(mockProfile)(dispatch)
                expect(LocationUtils.getLatLong).toHaveBeenCalledTimes(0)
                expect(backendProxy.getMatches).toBeCalledWith({
                    latLong: mockProfile.paidLocation,
                    radius: mockProfile.settings.maxDistance
                })
            })
        })

        describe("Distance should be transformed depending on the distanceUnit value", () => {

            beforeAll(() => {
                mockProfile.settings.distanceUnit = DistanceUnits.KM
                mockProfile.userType = UserTypes.NORMAL
            })
            afterAll(() => {
                mockProfile.settings.distanceUnit = DistanceUnits.KM
                mockProfile.userType = UserTypes.NORMAL
            })
            test("Calling with KM should not transform the maxDistance value", async () => {
                mockProfile.settings.distanceUnit = DistanceUnits.KM
                await getMatches(mockProfile)(dispatch)
                expect(backendProxy.getMatches).toBeCalledWith({
                    latLong: liveLocation,
                    radius: mockProfile.settings.maxDistance
                })
            })

            test("Calling with Miles should transfor to KM", async () => {
                mockProfile.settings.distanceUnit = DistanceUnits.MILES
                await getMatches(mockProfile)(dispatch)
                expect(backendProxy.getMatches).toBeCalledWith({
                    latLong: liveLocation,
                    radius: LocationUtils.transformKmToMi(mockProfile.settings.maxDistance)
                })
            })


        })

        test("getMatches action should dispatch a request_matches_success with right  params when get matches is successful", async () => {


            var fakeMatches = [{ userId: 1 }]
            jest.clearAllMocks()
            backendProxy.getMatches = mockBackendProxy.getMatches
            backendProxy.getMatches.mockReturnValueOnce(fakeMatches)
            await getMatches(mockProfile)(dispatch)
            expect(dispatch).toBeCalledWith({ type: 'request_matches_success', matches: fakeMatches })

        })




    })


    describe("reactToVideo action should call the right backend call and dispatch the right actions to the reducer", () => {

        var error, dispatch, rateIntent: RateIntent, requestAction, successAction, failureAction;

        beforeAll(() => {
            dispatch = jest.fn()
            rateIntent = {
                uri: 'a',
                userId: '1',
                videoId: 'bamba',
                rate: VideoRatings.GOOD
            }
            requestAction = {
                type: Actions.REQUEST_VIDEO_RATING,
                rateIntent: rateIntent
            }
            successAction = {
                type: Actions.REQUEST_VIDEO_RATING_SUCCESS,
                rateIntent: rateIntent
            }
            error = new Error('e')

            failureAction = {
                type: Actions.REQUEST_VIDEO_RATING_FAILURE,
                rateIntent: rateIntent,
                error: error
            }
        })


        beforeEach(() => {
            jest.clearAllMocks()
        })

        test("If backendProxy reactToVideo is succesfully it should dispatch success actions", async () => {
            const promise = new Promise(resolve => resolve())
            backendProxy.reactToVideo = mockBackendProxy.reactToVideo
            backendProxy.reactToVideo.mockReturnValueOnce(promise)
            await reactToVideo(rateIntent)(dispatch)
            expect(backendProxy.reactToVideo).toBeCalledWith(rateIntent)
            expect(dispatch).toBeCalledWith(requestAction)
            expect(dispatch).toBeCalledWith(successAction)
        })

        test("If backendProxy reactToVideo goes wrong it should dispatch failure actions", async () => {
            const promise = new Promise((resolve, reject) => { reject(error) })
            backendProxy.reactToVideo = mockBackendProxy.reactToVideo
            backendProxy.reactToVideo.mockReturnValueOnce(promise)
            await reactToVideo(rateIntent)(dispatch)
            expect(backendProxy.reactToVideo).toBeCalledWith(rateIntent)
            expect(dispatch).toBeCalledWith(requestAction)
            expect(dispatch).toBeCalledWith(failureAction)

        })



    })

    describe("setGender method tests", () => {
        let gender
        let requestAction
        let successAction
        let failureAction
        let error

        beforeAll(() => {
            gender = Genders.MALE
            requestAction = { type: Actions.REQUEST_SET_GENDER, gender }
            successAction = { type: Actions.REQUEST_SET_GENDER_SUCCESS, gender }
            error = new Error("Communication error")
            failureAction = { type: Actions.REQUEST_SET_GENDER_FAILURE, gender, error }

        })

        test(`It should call backendProxy setGender method with the gender and dispatch action with type ${Actions.REQUEST_SET_GENDER}`, () => {
            _mock(backendProxy.setGender).mockResolvedValue({})
            setGender(gender)(dispatch)
            expect(dispatch).toBeCalledWith(requestAction)
            expect(backendProxy.setGender).toBeCalledWith(gender)
        })

        describe("If backendProxy.setGender finishes succesfully", () => {
            beforeAll(() => {
                _mock(backendProxy.setGender).mockResolvedValue({})
            })

            test(`It should dispatch an action with ${Actions.REQUEST_SET_GENDER_SUCCESS} type`, async () => {
                await setGender(gender)(dispatch)
                expect(dispatch).toBeCalledWith(successAction)
            })
        })

        describe("If backendProxy.setGender finishes with error (promise rejects)", () => {

            beforeAll(() => {

                _mock(backendProxy.setGender).mockRejectedValue(error)
            })

            test(`It should dispatch an action with ${Actions.REQUEST_SET_GENDER_FAILURE} type`, async () => {
                await setGender(gender)(dispatch)
                expect(dispatch).toBeCalledWith(failureAction)
            })
        })







    })

    describe("popProfileSuggestion method tests", ()=>{
        popProfileSuggestion()(dispatch)
        
        expect(dispatch.mock.calls[0]).toMatchSnapshot()
    })

})