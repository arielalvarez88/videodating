//@flow 

import FirebaseRealTimeBackend from 'app/utils/backend/FirebaseRealTimeBackend'
import GeoFire from 'geofire';
import UserTypes from 'app/enums/UserTypes';
import { defaultProfile } from 'app/models/Profile';
import { updateVideo } from '../../../../app/stores/actionCreators/BackendActions';
import VideoRatings from '../../../../app/enums/VideoRatings';
import type { RateIntent } from 'app/stores/actionCreators/BackendActions'
import { defaultSettings } from '../../../../app/models/Settings';
import type { Settings } from 'app/models/Settings';
import ProfileUtils from 'app/utils/ProfileUtils';
import { DateTime } from 'luxon'
import Genders from 'app/enums/Genders'
import type { JestMockT } from "jest";

function _mock(mockFn) {
    return ((mockFn: any): JestMockT);
}

jest.mock('firebase', () => {
    let firebaseMock = require('firebase-mock');
    var cache = {}
    return firebaseMock.MockFirebaseSdk(function (path) {
        if (path in cache) {
            return cache[path]
        }
        cache[path] = new firebaseMock.MockFirebase(path)

        return cache[path]
    });


});





var backendProxy: FirebaseRealTimeBackend = new FirebaseRealTimeBackend()
var mockFirebaseUser = {
    uid: 'xx'
}
let mockProfile = Object.assign({}, defaultProfile, {
    id: mockFirebaseUser.uid,
    email: 'aa@aa.com',
    reactionsToVideos: {}
})



var userId1, userId2,
    profile1,
    profile2,
    matchingProfiles,
    ref1,
    ref2,
    myRef


describe('FirebaseRealTimeBackend tests', () => {


    beforeAll(() => {
        console.log('beforeAll');
        backendProxy.init()
        console.log('beforeAll2 ', backendProxy.db);
        backendProxy.user = mockFirebaseUser
        backendProxy.setMaxLocationMatches(2)
        backendProxy.setMaxMatches(2)

        userId1 = '1';
        userId2 = '2';
        profile1 = {
            name: 'rosa',
            facebookId: '1',
            email: 'aaaabbb@bb.com',
            principalVideo: {
                id: 'ugagua',
                uri: 'a',
                iuck: 1,
                bad: 1,
                good: 1,
                cool: 1,
                awesome: 1

            }
        };
        profile2 = {
            name: 'loca', facebookId: '2', email: 'bbb@bb.com',
            principalVideo: {
                id: 'ugagua2',
                uri: 'a2',
                iuck: 1,
                bad: 1,
                good: 1,
                cool: 1,
                awesome: 1

            }
        };
        matchingProfiles = [profile1, profile2];
        ref1 = backendProxy.db.ref(`/profiles/${userId1}`);
        ref2 = backendProxy.db.ref(`/profiles/${userId2}`);
        myRef = backendProxy.db.ref(`/profiles/${mockFirebaseUser.uid}`);

    })

    test("init geofire", () => {

        const locationRef = backendProxy.locationRef
        expect(backendProxy.geoFire.ref).toEqual(locationRef)
    })




    test("refreshLocation should call geoFire set with right params", () => {

        var coords = {
            latitude: 1,
            longitude: 2,
            altitude: 3,
            accuracy: 4,
            altitudeAccuracy: 5,
            heading: 6,
            speed: 7
        },
            location: any = {
                coords,
                timestamp: new Date().getTime()
            }

        backendProxy.updateLocation(location)

        expect(backendProxy.geoFire.set).toBeCalledWith(mockFirebaseUser.uid, [location.coords.latitude, location.coords.longitude])
    })

    describe("getMatches should subscribe to geoFire events for a number of matches and then stop listening to it", () => {


        beforeEach(() => {

            ref1.set(profile1)
            ref2.set(profile2)
            jest.spyOn(backendProxy, 'isMatch').mockReturnValue(true)
            

            console.log('beforeEach');

            //jest.useFakeTimers()
        })

        afterAll(() => {
            jest.restoreAllMocks()
        })

        test("We should save the geoQuery", () => {
            jest.useFakeTimers()
            backendProxy.getMatches({
                latLong: [1, 1],
                radius: 1
            })
            jest.runAllTimers();
            expect(backendProxy.lastGeoQuery).toBeTruthy()

        })

        describe("After a number of locationMatches or maxMatches (whicever is the smallest) the query will be cancelled and the matches promise resolve.", () => {

            beforeEach(() => {
                jest.clearAllMocks()

            })

            test("After X location matches, it should cancel the query", (done) => {
                backendProxy.setMaxLocationMatches(2)
                backendProxy.setMaxMatches(3)

                backendProxy.getMatches({
                    latLong: [1, 1],
                    radius: 1
                }).then(() => {
                    expect(backendProxy.lastGeoQuery.cancel).toBeCalled()
                    done()
                })


                if (backendProxy.lastGeoQuery) {
                    backendProxy.lastGeoQuery.emit('key_entered', userId1)
                    backendProxy.lastGeoQuery.emit('key_entered', userId2)
                    ref1.flush()
                    ref2.flush()
                }


            })

            test("After X matches, it should cancel the query", (done) => {
                backendProxy.setMaxLocationMatches(3)
                backendProxy.setMaxMatches(2)


                backendProxy.getMatches({
                    latLong: [1, 1],
                    radius: 1
                }).then(() => {
                    expect(backendProxy.lastGeoQuery.cancel).toBeCalledWith('done')
                    done()
                })

                if (backendProxy.lastGeoQuery) {
                    backendProxy.lastGeoQuery.emit('key_entered', userId1, 'userId1')
                    backendProxy.lastGeoQuery.emit('key_entered', userId2, 'userId2')
                    ref1.flush()
                    ref2.flush()


                }



            })

        })


        test("The response of getMatches should resolve to an array of profiles that matched", (done) => {
            backendProxy.setMaxLocationMatches(2)
            backendProxy.setMaxMatches(2)

            const matches = backendProxy.getMatches({
                latLong: [1, 1],
                radius: 1
            })
            matches.then(() => {

                done()
            })


            backendProxy.lastGeoQuery.emit('key_entered', userId1)
            backendProxy.lastGeoQuery.emit('key_entered', userId2)
            ref1.flush()
            ref2.flush()

            expect(matches).resolves.toMatchSnapshot()
            expect(matches).resolves.toEqual(matchingProfiles)


        })

        test("The response of getMatches should resolve after a timeout time has passed", (done) => {

            const matches = backendProxy.getMatches({
                latLong: [1, 1],
                radius: 1
            })
            matches.then(() => {
                done()
            })
            expect(matches).resolves.toEqual([])
            jest.runAllTimers();
        })

    })

    describe("rateVideoInOtherPersonsProfile should update the video in another persons profile", () => {

        var videoIntent: RateIntent,
            ref1Spy,
            promise,
            oldCount

        beforeAll(() => {
            videoIntent = {
                uri: profile1.principalVideo.uri,
                userId: userId1,
                rate: VideoRatings.GOOD,
                videoId: profile1.principalVideo.id,

            }
            oldCount = profile1.principalVideo[videoIntent.rate]
            ref1Spy = jest.spyOn(ref1, 'transaction')
            ref1.set(profile1)
            ref1.flush()
            promise = backendProxy.rateVideoInOtherPersonsProfile(videoIntent)
            ref1.flush()

        })

        test("Transaction was used to update the counters of the rate given in the other person's profile", (done) => {
            promise.then(() => {
                expect(ref1Spy).toBeCalled()
                done()
            })

        })

        describe("User video rating its count correctly", () => {

            test("If video uri was in principal video it should update it", (done) => {
                promise.then((ref) => {
                    expect(ref.snapshot.val().principalVideo[videoIntent.rate]).toBe(oldCount + 1)
                    done()
                })

            })

        })


        describe("updateMyRatings should update the video in my profile", () => {

            var rateIntent: RateIntent,
                ratingRef,
                refSpy,
                promise,
                oldCount

            beforeAll(() => {
                rateIntent = {
                    uri: profile1.principalVideo.uri,
                    userId: userId1,
                    videoId: profile1.principalVideo.id,
                    rate: VideoRatings.GOOD,
                }
                ratingRef = backendProxy.db.ref(`/profiles/${mockFirebaseUser.uid}/reactionsToVideos/${rateIntent.userId}/${rateIntent.videoId}`)

                refSpy = jest.spyOn(ratingRef, 'set')


            })
            test("Should call set on the Firebase reference to the rating with the rating value", () => {
                backendProxy.updateMyRatings(rateIntent)
                expect(refSpy).toBeCalledWith(rateIntent.rate)
            })



        })
    })

    describe("isMatch method tests", () => {
        describe("Filters are created from user Setting and applied", () => {

            let fakeAgeFilter
            let dummyFilter
            let fakeFilters
            let filterCreatorSpy
            let fakeProfile
            let response
            let settings
            beforeAll(() => {

                backendProxy.profile = defaultProfile

                settings = {
                    ...defaultSettings
                }
                backendProxy.profile.settings = settings

                fakeAgeFilter = { filter: jest.fn().mockReturnValue(true) }
                dummyFilter = { filter: jest.fn().mockReturnValue(true) }
                fakeFilters = [
                    fakeAgeFilter,
                    dummyFilter
                ]
                filterCreatorSpy = jest.spyOn(ProfileUtils, 'getFiltersFromSettings')
                filterCreatorSpy.mockReturnValueOnce(fakeFilters)

                fakeProfile = Object.assign({}, defaultProfile, {
                    birthday: DateTime.local().toFormat('M/d/yyyy')
                })
                response = backendProxy.isMatch(fakeProfile)
            })


            test("ProfileUtils.getFiltersFromSettings is used to create the filters from Setting", () => {
                expect(filterCreatorSpy).toBeCalledWith(settings)


            })


            test("All filters created should be applied", () => {
                fakeFilters.forEach((filter) => {
                    expect(fakeAgeFilter.filter).toBeCalledWith(fakeProfile)
                })

            })



            test("Is match should return a boolean that is an logic AND between all filters", () => {
                expect(response).toMatchSnapshot()
            })

            afterAll(() => {
                jest.restoreAllMocks()
            })


        })
    })

    describe("setGender method test", () => {
        test("It should set the value in the path /profiles/{profileId}/gender",()=>{
            const ref = backendProxy.db.ref(`/profiles/${backendProxy.user.uid}/gender`)
            const newGenderVal = Genders.MALE
            const refSpy = jest.spyOn(ref,'set')
            backendProxy.setGender(newGenderVal)            
            expect(refSpy).toBeCalledWith(newGenderVal)
        })
        


    })
})