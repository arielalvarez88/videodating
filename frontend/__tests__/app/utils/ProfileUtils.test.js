//@flow

import type { Filter } from 'app/utils/filters/Filter'
import ProfileUtils from 'app/utils/ProfileUtils'
import type { Settings } from '../../../app/models/Settings';
import { defaultSettings } from '../../../app/models/Settings';

describe("ProfileUtils test suite", () => {

    test("Should construct an age filter from user settings", () => {
        var userSettings: Settings = Object.assign({}, defaultSettings, {
            minAge: 18,
            maxAge: 35
        })
        const filters: Filter[] = ProfileUtils.getFiltersFromSettings(userSettings)

        expect(filters).toMatchSnapshot()
    })

    test("Should construct an gender filter from user settings", () => {
        var userSettings: Settings = Object.assign({}, defaultSettings, {
            showMeMen: false,
            showMeWomen: true,
        
        })
        const filters: Filter[] = ProfileUtils.getFiltersFromSettings(userSettings)

        expect(filters).toMatchSnapshot()
    })
})