import AgeFilter from 'app/utils/filters/AgeFilter'
import { DateTime } from 'luxon';

describe("Age filter test", ()=>{

    test("It should return true when the profile is in the age range (both ends inclusives), false otherwise", ()=> {
       const filter = new AgeFilter({
            maxAge: 30,
            minAge: 18
        });

        const inRange = {
            birthday : DateTime.local().minus({years: 18}).toString()
        }
        const notInRange = {

            birthday : DateTime.local().minus({years: 17}).toString()
        }
        const inRange2 = {
            birthday : DateTime.local().minus({years: 24}).toString()
        }
        const notInRange2 = {

            birthday : DateTime.local().minus({years: 9}).toString()
        }

        const inRange3 = {
            birthday : DateTime.local().minus({years: 30}).toString()
        }
        const notInRange3 = {

            birthday : DateTime.local().minus({years: 31}).toString()
        }
        expect(filter.filter(inRange)).toBe(true)
        expect(filter.filter(notInRange)).toBe(false)

        expect(filter.filter(inRange2)).toBe(true)
        expect(filter.filter(notInRange2)).toBe(false)

        expect(filter.filter(inRange2)).toBe(true)
        expect(filter.filter(notInRange2)).toBe(false)
    })
})