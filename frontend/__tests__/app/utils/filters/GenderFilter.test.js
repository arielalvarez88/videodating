import Genders from 'app/enums/Genders'
import GenderFilter from 'app/utils/filters/GenderFilter'

describe("GenderFilter tests", () => {

    describe("It should return true when the profile has a sex that the filter contains", () => {
        let maleProfile, femaleProfile

        beforeAll(() => {
            maleProfile = {
                gender: Genders.MALE
            }
            femaleProfile = {
                gender: Genders.FEMALE
            }
        })


        
        test("If filter has male gender only, filtering a male profile should return true", () => {
            const maleGenderFilter = new GenderFilter([Genders.MALE])
            expect(maleGenderFilter.filter(maleProfile)).toBe(true)
        }) 
        
        test("If filter has female gender only, filtering a female profile should return true", () => {
            const femaleGenderFilter = new GenderFilter([Genders.FEMALE])
            expect(femaleGenderFilter.filter(femaleProfile)).toBe(true)
        })

        test("If filter has female gender only, filtering a male profile should return false", () => {
            const femaleGenderFilter = new GenderFilter([Genders.FEMALE])
            expect(femaleGenderFilter.filter(maleProfile)).toBe(false)
        })

        test("If filter has male gender only, filtering a female profile should return false", () => {
            const maleGenderFilter = new GenderFilter([Genders.MALE])
            expect(maleGenderFilter.filter(femaleProfile)).toBe(false)
        })


        test("If filter has male and female genders, filtering a female profile should return true", () => {
            const maleAndFemaleGenderFilter = new GenderFilter([Genders.MALE, Genders.FEMALE])
            expect(maleAndFemaleGenderFilter.filter(femaleProfile)).toBe(true)
        })

        test("If filter has male and female genders, filtering a male profile should return true", () => {
            const maleAndFemaleGenderFilter = new GenderFilter([Genders.MALE, Genders.FEMALE])
            expect(maleAndFemaleGenderFilter.filter(maleProfile)).toBe(true)
        })

    })
})