//@flow
declare var jest: any
const EventEmitter = require('events');

class GeoQueryMock extends EventEmitter {
    cancel = jest.fn()
}
export default class GeoFireMock {
    queryEvent = new GeoQueryMock()
    ref: any
    
    constructor(ref: any) {
        this.ref = ref;
    }

    set = jest.fn()
    query = jest.fn().mockImplementation(() => {
        this.queryEvent =  new GeoQueryMock()
        return this.queryEvent
    })

    
}