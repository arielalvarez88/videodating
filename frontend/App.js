/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import 'babel-polyfill';
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { Provider } from 'react-redux';
import Login from './app/components/login/Login';
import store from './app/stores/AppStore';
import backendProxy from './app/utils/backend/BackendProxy';
import GenderSelectionOverlay from 'app/components/genderSelectionOverlay/GenderSelectionOverlay'
import RootStack from './app/components/navigation/RootStack';
import SpinnerComponent from './app/components/spinnerComponent/SpinnerComponent';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
        'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

console.ignoredYellowBox = ['Setting a timer']


type Props = {};
export default class App extends Component<Props> {

    componentWillMount() {

        backendProxy.init();
    }

    render() {
        return (
            <Provider store={store}>              
                <View flex={1} alignItems={"stretch"}>
                    <GenderSelectionOverlay/>
                    <SpinnerComponent />
                    <RootStack flex={1} />
                </View>
            </Provider>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
