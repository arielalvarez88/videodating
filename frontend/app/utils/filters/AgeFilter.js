//@flow

import { calculateAge } from 'app/utils/social/SocialUtils'
import type { Profile } from 'app/models/Profile'

export default class AgeFilter {


    minAge: number = 18
    maxAge: number = 24

    constructor({ minAge, maxAge }: { minAge: number, maxAge: number }) {
        this.minAge = minAge
        this.maxAge = maxAge
    }

    filter = (profile: Profile) => {
        const profileAge: number = calculateAge(profile.birthday)
        return profileAge >= this.minAge && profileAge <= this.maxAge
    }
}