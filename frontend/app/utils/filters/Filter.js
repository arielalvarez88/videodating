export interface Filter {
    filter(model: any): boolean;
}