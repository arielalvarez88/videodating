//@flow

import { calculateAge } from 'app/utils/social/SocialUtils'
import type { Profile } from 'app/models/Profile'
import type {Gender} from 'app/enums/Genders'

export default class GenderFilter {

    validationMap: Object = {}

    constructor(genders: Gender[]) {
        genders.forEach((gender) => {
            this.validationMap[gender] = true
        })
    }

    filter = (profile: Profile): boolean => {
        const gender: string = profile.gender
        return Boolean(this.validationMap[gender])

    }
}