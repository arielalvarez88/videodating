// @flow 

// import type BackendProxy from './BackendProxy';
import * as firebase from 'firebase';
import 'firebase/firestore';
import type { BackendProxy } from 'app/utils/backend/BackendProxy';
import TimeoutError from '../../exceptions/TimeoutError';
import type { Profile } from '../../models/Profile';
import GeoFire from 'geofire';
import type { Settings } from '../../models/Settings';
import { defaultSettings } from '../../models/Settings';
import { defaultProfile } from '../../models/Profile';
import RNFetchBlob from 'react-native-fetch-blob';
import {
    Platform,
} from 'react-native';
import { path } from 'app/utils/functionalProgramming/F'
import EventEmitter from 'events';
import { defaultVideo } from 'app/models/VideoInfo';
import type { VideoInfo } from 'app/models/VideoInfo';
import type { RateIntent } from '../../stores/actionCreators/BackendActions';
import StringUtils from '../StringUtils';
import ProfileUtils from 'app/utils/ProfileUtils'
import type { Filter } from 'app/utils/filters/Filter'
import type { Gender } from 'app/enums/Genders'

type LocationQuery = ?EventEmitter & {
    cancel: Function,
    on: Function
}
export default class FireBaseRealTimeBackend implements BackendProxy {

    db: firebase.database.Database
    info: any = {
        apiKey: 'AIzaSyBABh776n5gfhWL7Rn7PsGKzM279l226FQ',
        authDomain: 'video-dating-68aeb.firebaseapp.com',
        databaseURL: 'https://video-dating-68aeb.firebaseio.com/',
        storageBucket: 'video-dating-68aeb.appspot.com',
    }
    app: firebase.app.App
    user: firebase.User
    profile: ?Profile
    logged: boolean = false
    geoFire: GeoFire
    locationRef: firebase.database.Reference
    lastGeoQuery: LocationQuery & EventEmitter
    matchesTimeout: number = 10000

    /**
     * @property {number} locationMatches
     * Number of location matches before stop searching.
     * Location matches are users that meet only the distance criteria.
     */
    maxLocationMatches: number = 2

    /**
     * @property {number} locationMatches
     * Number of location matches in last search
     */
    locationMatches: number = 0

    /**
     * @property {number} maxMatches
     * Max number of matches per search.
     */
    maxMatches: number = 100

    /**
     * Number of matches in the last search.
     */
    matches: number

    init = () => {

        this.app = firebase.initializeApp(this.info)

        this.db = firebase.database()
        this.locationRef = this.db.ref('location')
        this.geoFire = new GeoFire(this.locationRef)


    }

    isLogged = (): Promise<boolean> => {
        const promise = new Promise(this._isLogged)
        return promise
    }

    _isLogged = (resolve: Function, reject: Function) => {

        const clearTimeoutId = setTimeout(() => { reject(new TimeoutError()) }, 400)

        firebase.auth().onAuthStateChanged((user: ?string) => {
            clearTimeout(clearTimeoutId)
            if (user) {
                this.logged = true
                resolve(true)

            } else {
                this.logged = false
                resolve(false)
            }
        });

    }

    updateLocation = (coordinates: Position): Promise<any> => {
        let { latitude, longitude } = coordinates.coords
        return this.geoFire.set(this.user.uid, [latitude, longitude]);


    }

    getProfile = async (userId: string = this.user.uid): Promise<?Profile> => {

        let result: any = await this.db.ref(`/profiles/${userId}`).once('value')
        result = result.val()
        const profile: ?Profile = result && result.email ? result : null

        return profile
    }

    login = async (profile: Profile): Promise<firebase.User> => {
        const credential = firebase.auth.FacebookAuthProvider.credential(profile.token)

        this.user = await firebase.auth().signInWithCredential(credential)
        profile.id = this.user.uid
        this.profile = await this.getProfile(this.user.uid)

        if (!this.profile) {

            this.profile = await this.createProfile(profile)


        }

        this.logged = true
        return this.user;

    }

    createProfile = async (profile: Profile): Promise<Profile> => {

        await this.db.ref(`/profiles/${this.user.uid}`).set(profile)
        return profile
    }



    async getProfileSettings(): Promise<?Settings> {
        const profile: ?Profile = await this.getProfile();

        return profile ? profile.settings : null;
    }

    async updateSettings(partialSettings: Object): Promise<any> {
        let pathsToUpdate = {}

        Object.keys(partialSettings).forEach(fieldName => {
            pathsToUpdate[`/profiles/${this.user.uid}/settings/${fieldName}`] = partialSettings[fieldName]
        })

        return this.db.ref().update(pathsToUpdate)
    }

    async updateProfile(propertiesToUpdate: Object) {


        return this.db.ref(`/profiles/${this.user.uid}`).update(propertiesToUpdate)

    }

    uploadVideo = async (uri: string, videoName: string = 'principal', mime: string = 'video/mp4'): Promise<string> => {

        const Blob = RNFetchBlob.polyfill.Blob;
        window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
        window.Blob = Blob;

        const fs = RNFetchBlob.fs
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri


        let rnfbURI = RNFetchBlob.wrap(uploadUri);

        const videoBlob = await Blob.build(rnfbURI, { type: mime })

        const id = StringUtils.generateUID()
        const videoRef = this.app.storage().ref(`/videos/${this.user.uid}/${id}`)

        await videoRef.put(videoBlob, { contentType: mime })
        const videoURL = await videoRef.getDownloadURL()
        let videoInfo: VideoInfo = {
            id,
            uri: videoURL
        }
        videoInfo = Object.assign({}, defaultVideo, videoInfo)
        await this.updateProfile({ principalVideo: videoInfo })
        return videoInfo
    }

    setMaxLocationMatches = (maxLocationMatches: number) => {
        this.maxLocationMatches = maxLocationMatches
    }

    setMaxMatches = (maxMatches: number) => {
        this.maxMatches = maxMatches
    }

    isMatch = (profileToCheckIfMatch: Profile): boolean => {
        if (!this.profile || !this.profile.settings) {
            return false;
        }
        const hasAVideo = profileToCheckIfMatch.principalVideo.uri
        const mySettings = this.profile.settings
        const filters: Filter[] = ProfileUtils.getFiltersFromSettings(mySettings)
        const passesFilters = filters.reduce((acc, filter) => {
            return acc && filter.filter(profileToCheckIfMatch)
        }, true)

        return passesFilters && hasAVideo

    }

    getMatches = (location: { latLong: [number, number], radius: number }): Promise<Profile[]> => {
        return new Promise((resolve, reject) => {

            
            const matches: Profile[] = []
            const geoQuery: LocationQuery = this.startLocationQuery(location)
            const timeoutId = setTimeout(() => {
                
                this.stopSearchingMatches(geoQuery,'timeout')
                resolve(matches.slice())
            }, this.matchesTimeout)

            geoQuery.on('key_entered', async (matchUserId: string, deleteMe: string, matchLangLong: [number, number]) => {


                const matchProfile: ?Profile = await this.getProfile(matchUserId)


                if (matchProfile) {

                    const isMatch = this.isMatch(matchProfile)

                    if (isMatch) {
                        this.matches++
                        matches.push(matchProfile)
                    }
                }
                this.locationMatches++


                if (this.locationMatches >= this.maxLocationMatches || this.matches >= this.maxMatches) {

                    clearTimeout(timeoutId)
                    this.stopSearchingMatches(geoQuery,'done')
                    resolve(matches.slice())
                }
            })

        })
    }

    startLocationQuery = (location: { latLong: [number, number], radius: number }): LocationQuery => {
        if (this.lastGeoQuery) {
            this.lastGeoQuery.cancel()
        }
        this.locationMatches = 0
        this.matches = 0
        this.lastGeoQuery = this.geoFire.query({
            center: location.latLong,
            radius: location.radius
        })
        return this.lastGeoQuery
    }

    stopSearchingMatches = (geoQuery : LocationQuery = this.lastGeoQuery, reason: string) => {
        geoQuery.cancel(reason)
        this.locationMatches = 0
        this.matches = 0
    }

    setMatchesTimeout = (milliSecs: number) => {
        this.matchesTimeout = milliSecs
    }

    reactToVideo = async (rateIntent: RateIntent): Promise<any> => {
        await this.rateVideoInOtherPersonsProfile(rateIntent)
        await this.updateMyRatings(rateIntent)
    }

    rateVideoInOtherPersonsProfile = async (rateIntent: RateIntent) => {
        const ref = this.db.ref(`/profiles/${rateIntent.userId}`)


        const updating = ref.transaction(userRef => {
            const otherPersonsVideoUri = path(['principalVideo', 'uri'], userRef)
            if (otherPersonsVideoUri && otherPersonsVideoUri === rateIntent.uri) {
                userRef.principalVideo[rateIntent.rate]++
            }
            return userRef
        })


        return updating

    }


    setProfile = (profile: Profile) => {
        this.profile = profile
    }

    updateMyRatings = (rateIntent: RateIntent): Promise<any> => {
        const refToMyRatings = this.db.ref(`/profiles/${this.user.uid}/reactionsToVideos/${rateIntent.userId}/${rateIntent.videoId}`)
        try{
            return refToMyRatings.set(rateIntent.rate)
        }catch(error){
            return Promise.reject(error)
        }
        
    }

    setGender = (gender: Gender): Promise<any> => {
        return this.db.ref(`/profiles/${this.user.uid}/gender`).set(gender)
    }

}
