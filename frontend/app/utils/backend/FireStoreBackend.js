// import type BackendProxy from './BackendProxy';
import * as firebase from 'firebase';
import 'firebase/firestore';
import type { BackendProxy } from 'app/utils/backend/BackendProxy';
import TimeoutError from '../../exceptions/TimeoutError';
import type { Profile } from '../../models/Profile';

import type { Settings } from '../../models/Settings';

export default class FireStoreBackend implements BackendProxy {

    db: firebase.firestore.Firestore
    info: any = {
        apiKey: 'AIzaSyBABh776n5gfhWL7Rn7PsGKzM279l226FQ',
        authDomain: 'video-dating-68aeb.firebaseapp.com',
        projectId: 'video-dating-68aeb',
        storageBucket: '<BUCKET>.appspot.com',
    }
    app: firebase.app.App
    user: firebase.User
    profile: ?Profile
    logged: boolean = false

    init() {

        this.app = firebase.initializeApp(this.info)
        // Initialize Cloud Firestore through Firebase
        this.db = firebase.firestore()
        firebase.firestore.setLogLevel('debug')

    }

    isLogged(): Promise<boolean> {
        const promise = new Promise(this._isLogged)
        return promise
    }

    _isLogged(resolve: Function, reject: Function) {

        const clearTimeoutId = setTimeout(() => { reject(new TimeoutError()) }, 400)

        firebase.auth().onAuthStateChanged((user: ?string) => {
            clearTimeout(clearTimeoutId)
            if (user) {
                this.logged = true
                resolve(true)

            } else {
                this.logged = false
                resolve(false)
            }
        });

    }

    async getProfile(userId: string = ''): Promise<?Profile> {
        if (this.profile) {
            return this.profile
        }

        const profile: firebase.firestore.DocumentSnapshot = await this.db.collection('profiles').doc(userId).get()
        if (profile.exists) {
            this.profile = profile
        }

        return this.profile ? this.profile : null
    }

    async login(profile: Profile): Promise<firebase.User> {
        const credential = firebase.auth.FacebookAuthProvider.credential(profile.token)

        this.user = await firebase.auth().signInWithCredential(credential)
        this.profile = await this.getProfile(this.user.uid)

        if (!this.profile) {
            this.profile = (await this.createProfile(profile) : Profile)
        }

        this.logged = true
        return this.user;

    }

    async createProfile(profile: Profile) {
        return this.db.collection("profiles").add(profile.email).set(profile)
    }

    async getProfileSettings(): Promise<?Settings> {
        const profile: ?Profile = await this.getProfile();

        return profile ? profile.settings : null;
    }


}
