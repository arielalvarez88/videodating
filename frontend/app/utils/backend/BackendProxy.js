// @flow


import FireBaseRealTimeBackend from './FireBaseRealTimeBackend'
import type { Profile } from '../../models/Profile';
import type { VideoInfo } from '../../models/VideoInfo';
import type { RateIntent } from 'app/stores/actionCreators/BackendActions'
import type { Gender } from 'app/enums/Genders'
export interface BackendProxy {


    login(userInfo: any): Promise<Profile>;
    getProfile(userId: string): Promise<?Profile>;
    updateProfile(propertiesToUpdate: Object): Promise<any>;
    createProfile(profile: Profile): Promise<Profile>;
    updateSettings(partialSettings: Object): Promise<any>;
    getMatches({ latLong: [number, number], radius: number }): Promise<Profile[]>;
    setMaxMatches(maxMatches: number): void;
    setMaxLocationMatches(maxLocationMatches: number): void;

    /**
     * Uploads a video and returns a promis that resolves into a the url to download the video.
     * 
     * @param {string} uri 
     * @param {string} videoName 
     * @param {string} mime 
     * @returns {Promise<string>} 
     */
    uploadVideo(uri: string, videoName: string, mime: string): Promise<VideoInfo>;
    /**
     * Rate video with specified uri in the specified user
     * 
     * @param {RateIntent} rateIntent     
     * 
     */
    reactToVideo(rateIntent: RateIntent): Promise<any>;

    setProfile(profile: Profile): void;

    /**
     * Updates gender in backend.
     * @param {Gender} gender 
     */
    setGender(gender: Gender): Promise<any>;
}

const proxy: FireBaseRealTimeBackend = new FireBaseRealTimeBackend()
export default proxy;
