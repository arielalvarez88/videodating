import GeoFire from 'geofire'
import type {JestMockT} from "jest"
class GeoFireMock {
    geoFire = new GeoFire()

    getMatches : JestMockT = jest.fn().mockImplementation((location) => {
        this.geoFire.query(location)
    })
    reactToVideo = jest.fn().mockImplementation((intent)=>{
        return new Promise(resolve=>resolve())
    })

    setGender = jest.fn()
}
export default new GeoFireMock()