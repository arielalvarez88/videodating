//@flow 

import { PermissionsAndroid } from 'react-native';
type Permission = $Values<typeof PermissionsAndroid.PERMISSIONS>

import { safeProp } from 'app/utils/functionalProgramming/F'


const hasPermission = (permissionName: Permission): Promise<boolean> => {

    return PermissionsAndroid.check(permissionName)

}

const hasLocationPermission = (): Promise<boolean> => {
    return hasPermission(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
}


const requestLocationPermission = async (): Promise<boolean> => {
    const permissionName = PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    const granted = await PermissionsAndroid.request(
        permissionName,
        {
            'title': 'Cool Photo App Camera Permission',
            'message': 'Cool Photo App needs access to your camera ' +
                'so you can take awesome pictures.'
        }
    )


    return granted === PermissionsAndroid.RESULTS.GRANTED;

}

export { hasPermission, hasLocationPermission, requestLocationPermission }
