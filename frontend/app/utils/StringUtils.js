import camelcase from 'camelcase'
import uuid4 from 'uuid/v4';

export default class StringUtils {
    static generateUID(){
        return uuid4()
    }
    static toCamelCase(val: string): string {
        return camelcase(val)
    }
}
