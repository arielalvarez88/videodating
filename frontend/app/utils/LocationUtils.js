//@flow

import { curry } from "app/utils/functionalProgramming/F"
import type { Task } from "app/utils/functionalProgramming/F"
import { task, of } from 'folktale/concurrency/task';



export default class LocationUtils {

    static transformKmToMi = (kmDistance: number): number => {
        return 0.62 * kmDistance;
    }

    static getLocation = async (): Promise<Position> => {



        const success = curry((resolve, postion: Position) => {
            return resolve(postion)
        })

        return new Promise((resolve, reject) => {

            navigator.geolocation.getCurrentPosition(success(resolve), error => reject(error))
        })

    }

    static getLatLong = async (): Promise<[number, number]> => {



        const location = await LocationUtils.getLocation()
        let latLong = [location.coords.latitude, location.coords.longitude]

        return latLong

    }
}