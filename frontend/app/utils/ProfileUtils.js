//@flow
import type { Settings } from "app/models/Settings";
import AgeFilter from "app/utils/filters/AgeFilter";
import GenderFilter from "app/utils/filters/GenderFilter";
import type { Filter } from "app/utils/filters/Filter";
import Genders from "app/enums/Genders";


const getFiltersFromSettings = (profileSettings: Settings): Filter[] => {
    const ageFilter: Filter = createAgeFilter(profileSettings)
    const genderFilter : Filter = createGenderFilter(profileSettings)
    return [ageFilter, genderFilter]
}
const createAgeFilter = (profileSettings: Settings): Filter => {
    return new AgeFilter({ minAge: profileSettings.minAge, maxAge: profileSettings.maxAge })
}
const createGenderFilter = (profileSettings: Settings): Filter => {
    const genders = []
    if(profileSettings.showMeMen){
        genders.push(Genders.MALE)
    }
    if(profileSettings.showMeWomen){
        genders.push(Genders.FEMALE)
    }
    return new GenderFilter(genders)
}

export default {getFiltersFromSettings}