//@flow

import R from 'ramda';
import Result from 'folktale/result';
import maybe from 'folktale/maybe';

type Task<T> = {
    run: Function

}

const head = R.head

interface Functor {
    value: any;
    map(f: Function): Functor;
}


class Maybe {
    value: any = null

    constructor(x: any) {
        this.value = x
    }

    static of = (x: any) => {
        return new Maybe(x)
    }

    map = (f: Function) => {
        if (this.isNothing()) {
            return new Maybe(null)
        } else {
            return new Maybe(f(this.value))
        }

    }
    isNothing = () => {
        return typeof this.value === "undefined" || this.value === null
    }
}


const compose = R.compose;

interface IMonad extends Functor {

    join(): IMonad | any;
}
interface staticsMonad {
    of(x: any): Functor;
}

class Monad implements IMonad {
    value: any = null

    //@template override me!
    map = (f: Function) => {
        throw new Error("You need to implment map function");
    }

    //@template override me!
    join = () => {
        throw new Error("You need to implment join function");
    }

    chain = (f: Function) => {
        return join(this.map(f))
    }
}


const Left = Result.Error
const Right = Result.Ok
const curry = R.curry
const map = R.map
// Monad :: => Monad(Monad (a))-> Monad(a)
const join = (x: Monad) => {
    return x.join()
}


const IO = function (x: any) {
    this.unsafePerformIO = x
}

IO.of = function (x: any) {
    return new IO(() => x)
}

IO.prototype.map = function (f) {
    return new IO(compose(f, this.unsafePerformIO))

}

//  safeProp :: Key -> {Key: a} -> Maybe a
var safeProp = curry(function (x, obj) {
    return new Maybe(obj[x]);
});

const either = curry(function (f, g, x) {
    if (x instanceof Left) {
        return f(x.value)
    } else {
        return g(x.value)
    }
})

// consta :: Monad m => -> (a -> mb) -> ma -> mb
const chain = curry((f, ma) => {
    ma.map(f).join()
})

const safePath = compose(maybe.fromNullable, R.path)
const path = curry((path: any[], obj: Object) : any => {
    return R.path(path, obj)
})

export { IO, Left, Right, Maybe, head, curry, map, join, compose, either, chain, safeProp, safePath, path }
export type { Task }