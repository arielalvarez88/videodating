//@flow

import { curry, IO } from './F'

const push = curry((arr: Array<any>, value: any) => {
    return new IO((): void => {
        arr.push(value)
    })
})

export { push }