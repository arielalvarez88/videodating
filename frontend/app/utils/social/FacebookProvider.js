// @flow

import { SocialProvider } from './SocialProvider';
import StringUtils from '../StringUtils';
import type { UserData } from './UserData';
import type { SocialProviderName } from './SocialProviderName';
// import { GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { GraphRequest, GraphRequestManager, AccessToken } from 'react-native-fbsdk';


export default class FacebookProvider implements SocialProvider {

    profileOptions: Object;
    loginData: Object;
    id: ?string = null;

    constructor(loginData: any, profileOptions: any) {
        this.id = null
        this.loginData = loginData
        this.profileOptions = Object.assign({}, profileOptions, {

            fields: 'id,name,first_name,last_name,birthday,gender,email',
        })
    }

    async mapToUserData(providerSpecificResponse: any): Promise<UserData> {
        const data: any = {
            token: {}
        };

        Object.keys(providerSpecificResponse).forEach((key: any) => {
            data[StringUtils.toCamelCase(key)] = providerSpecificResponse[key]
        })

        data.userId = providerSpecificResponse.id
        try {
            data.token = await AccessToken.getCurrentAccessToken()
            if (!data.token) {
                throw new Error('Token not present')
            }
            data.token = data.token.accessToken
        } catch (err) {
            console.log(err);
            throw err;
        }


        data.provider = 'facebook'
        data.birthday = new Date(providerSpecificResponse.birthday)
        data.logged = true
        const castedUserData: UserData = (data: UserData)

        return castedUserData
    }

    getUserData(): Promise<UserData> {
        const me = this;
        const promiseToReturn = new Promise((resolve: Function, reject: Function) => {

            const infoRequest = new GraphRequest(
                `/me?fields=${this.profileOptions.fields}`,
                this.profileOptions,
                async (error: ?Object, data: ?Object): any => {
                    if (error) {
                        reject(error)
                    }
                    const userData = await me.mapToUserData(data)
                    resolve(userData)

                },

            );

            new GraphRequestManager().addRequest(infoRequest).start();
        });


        return promiseToReturn


    }


    graphApiCallback(resolve: Function, reject: Function, error: ?Object, data: ?Object): any {
        if (error) {
            reject(error)
            return
        }

        resolve(data)
    }

    getProviderName = (): SocialProviderName => 'facebook'


}
