// @flow

import { SocialProvider } from './SocialProvider';
import FacebookProvider from './FacebookProvider';


export default class SocialProviderFactory {
    static getInstance(providersName: string, loginResp: ?any): SocialProvider {
        switch (providersName) {
        case 'facebook':
            return new FacebookProvider()
        default:
            return new FacebookProvider()
        }
    }
}
