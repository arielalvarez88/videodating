// @flow

export type SocialProviderName = "facebook" | "instagram"
