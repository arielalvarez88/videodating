// @flow

import type { UserData } from './UserData'
import type { SocialProviderName } from "./SocialProviderName";

export interface SocialProvider {
    getUserData(): Promise<UserData>;
    getProviderName(): SocialProviderName;
}
