// @flow

export type UserData = {
    userId: string;
    name: string;
    firstName: string;
    middleName: string;
    lastName: string;
    birthday: Date;
    gender: string;
    email: string;
    logged: boolean;
    provider: string;
    token: string;
};
