//@flow

import type { SocialProviderName } from '../../utils/social/SocialProviderName';
import type { UserData } from './UserData';
import { DateTime } from 'luxon';
import type { Profile } from '../../models/Profile';
import { defaultProfile } from "app/models/Profile"

const mapUserDataToProfile = (providerName: SocialProviderName, data: UserData): Profile => {

    const user: any = {};
    Object.assign(user, defaultProfile, data)
    const userDataToProfileMap = {
        "name": "displayName"
    }

    Object.keys(userDataToProfileMap).forEach(key => {
        user[userDataToProfileMap[key]] = data[key]
    })



    switch (providerName) {
        case 'facebook':
            user.facebookId = data.userId
            break;
        default:
            user.facebookId = data.userId
    }

    //Transform dates to string so firebase can store it.
    Object.keys(user).forEach(key => {
        if (user[key] instanceof Date) {
            user[key] = user[key].toString()
        }
    })

    return (user: Profile);

}

const calculateAge = (birthday: string): number => {
    //DateTime.fromFormat(birthday, 'MM/dd/yyyy').diff(DateTime.local(), 'years')
    const brithdayDate = new Date(birthday)
    return Math.floor(DateTime.local().diff(DateTime.fromJSDate(brithdayDate), 'years').years)
}

export { mapUserDataToProfile, calculateAge }