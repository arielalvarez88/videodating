import {StyleSheet} from 'react-native'

const ButtonStyles = {
    selected : {
        backgroundColor: 'green',
        color: 'white',
        padding: 10
    },
    unselected : {
        backgroundColor: 'white',
        color: 'green',
        padding: 10
    }
}

export {ButtonStyles}