//@flow

const Genders = Object.freeze({
    MALE: 'm',
    FEMALE: 'f'
})
export type Gender = $Values<typeof Genders>

export default Genders;
