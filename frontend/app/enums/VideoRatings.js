//@flow

/**
 * @typedef {String} VideoRatings
 * 
 */
const VideoRatings = Object.freeze({
    IUCK: 'iuck',
    BAD: 'bad',
    GOOD: 'good',
    COOL: 'cool',
    AWESOME: 'awesome'
})

export type VideoRating = $Values<typeof VideoRatings>

export default VideoRatings;
