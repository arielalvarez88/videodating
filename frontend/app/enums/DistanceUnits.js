//@flow

const DistanceUnits = Object.freeze({
    MILES: 'mi',
    KM: 'km'
})
export type DisntanceUnit = $Values<typeof DistanceUnits>

export default DistanceUnits;
