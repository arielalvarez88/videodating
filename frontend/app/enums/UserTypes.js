//@flow

const UserTypes = Object.freeze({
    PREMIUM: 'premium',
    NORMAL: 'normal'
})
export type UserType = $Values<typeof UserTypes>

export default UserTypes;
