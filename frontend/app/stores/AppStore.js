// @flow

import { combineReducers, createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import promiseMiddleware from 'redux-promise';
import thunkMiddleware from 'redux-thunk';
import NavigationReducer from './reducers/Navigation';
import timeoutHandler from './middleware/TimeoutHandler';
import BackendReducer from './reducers/Backend';
import PermissionsReducer from 'app/stores/reducers/Permissions';
import backendProxy from 'app/utils/backend/BackendProxy'
import NotificationReducer from './reducers/Notification';
import backendReducer from './reducers/Backend';
import {safePath} from 'app/utils/functionalProgramming/F'

const reducer = combineReducers({
    Navigation: NavigationReducer,
    Backend: BackendReducer,
    Notification: NotificationReducer,
    Permissions: PermissionsReducer

})

const store = createStore(reducer, applyMiddleware(logger, promiseMiddleware, thunkMiddleware, timeoutHandler))

store.subscribe(() => {
    safePath(['Backend', 'profile'], store.getState()).map(profile => {
        backendProxy.setProfile(profile)
    })
})

export default store;
