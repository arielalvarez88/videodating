// @flow

const NavigationReducer = (state: string = 'Login', action: Object): string => {

    switch (action.type) {
        case 'navigate_main':
        
            if (action.firstLogin || true) {
                action.navigate('Settings')
                return 'Settings'                
            } else {
                action.navigate('Rate')
                return 'Rate'
            }
        case 'navigate' :
            action.navigate(action.section)    
            return action.section
        case 'navigate_settings':
            action.navigate('Settings')
            return 'Settings'
        default:
            return state


    }

}

export default NavigationReducer;

