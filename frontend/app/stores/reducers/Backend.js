// @flow 
import { Alert } from 'react-native';
import { SocialProvider } from '../../utils/social/SocialProvider';
import type { Profile } from 'app/models/Profile'
import Actions from '../Actions'
import { path } from 'app/utils/functionalProgramming/F'

type BackendState = {
    logged: boolean,
    socialProvider: ?SocialProvider,
    profile: ?Profile,
    fetchingMatches: boolean,
    lastSuggestions: ?Profile[]
}
const defaultState: BackendState = {
    logged: false,
    socialProvider: null,
    firebaseUser: null,
    profile: null,
    fetchingMatches: false,
    lastSuggestions: []
}

const backendReducer = (state: any = defaultState, action: Object) => {

    if (action.type.indexOf('_failure') > 0) {
        Alert.alert('Communication Error', 'We are experiencing problems. Please try again later.')
        console.error(action.error, "State ->", state, "Action dispatched ->", action)
        return state;
    }

    const actionCopy = { ...action }
    delete actionCopy.type


    switch (action.type) {
        case "request_login_success":
            state = {
                ...state,
                ...actionCopy,
                logged: true
            }
            return state
        case "request_update_settings_success":

            const settings = {}
            Object.assign(settings, state.profile.settings, actionCopy)
            state = {
                ...state,
                profile: {
                    ...state.profile,
                    settings: settings
                }
            }
            return state
        case "request_update_profile_success":
            return {
                ...state,
                profile: {
                    ...state.profile,
                    ...action.update
                }

            }
        case "request_upload_video_success":
            return {
                ...state,
                profile: {
                    ...state.profile,
                    principalVideo: action.videoInfo,
                    videos: [
                        action.videoInfo
                    ]
                }

            }
        case "request_vd_google_signin_success":
            return {
                ...state,
                VDLoggedToGoogle: true,
                VDGoogleUser: action.user
            }
        case "request_matches":
            return {
                ...state,
                fetchingMatches: true
            }
        case "request_matches_success":
            return {
                ...state,
                fetchingMatches: false,
                lastSuggestions: action.matches
            }
        case "request_matches_failure":
            return {
                ...state,
                fetchingMatches: false
            }
        case Actions.REQUEST_VIDEO_RATING_SUCCESS:
            const myReactions = path(['profile', 'reactionsToVideos', action.rateIntent.userId], state)

            return {
                ...state,
                profile: {
                    ...state.profile,
                    reactionsToVideos: {
                        ...state.profile.reactionsToVideos,
                        [action.rateIntent.userId]: {
                            ...(myReactions || {}),
                            [action.rateIntent.videoId]: action.rateIntent.rate
                        }
                    }
                }
            }
        case Actions.REQUEST_SET_GENDER_SUCCESS:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    gender: action.gender
                }
            }
        case Actions.POP_PROFILE_SUGGESTION:
            return {
                ...state,
                lastSuggestions: state.lastSuggestions.slice(1)
            }
        default:
            return state
    }

}
export default backendReducer