// @flow

type NotificationState = {
    loading: false,
    loadingMessage: string
}
const NotificationReducer = (state: any, action: Object): Object => {

    state = state || {
        loading: false,
        loadingMessage: ''
    }

    switch (action.type) {
        case 'set_loading':
            return {
                ...state,
                loading: action.loading,
                loadingMessage: action.message
            }

        default:
            return state


    }

}

export default NotificationReducer;

