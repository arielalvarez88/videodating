// @flow
import { PermissionsAndroid } from 'react-native';

type PermissionsState = {
    [PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION]: boolean
}
const defaultState: PermissionsState = {
    [PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION]: false
}

const PermissionReducer = (state: any = defaultState, action: { type: string, permissionName: string }): string => {

    switch (action.type) {
        case 'permission_denied':
            return {
                ...state,
                [action.permissionName]: false
            }
        case 'permission_granted':
            return {
                ...state,
                [action.permissionName]: true
            }
        default:
            return state

    }

}

export default PermissionReducer;

