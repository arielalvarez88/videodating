import TimeoutError from '../../exceptions/TimeoutError';

const timeoutMiddleware = store => next => (action) => {
    if (action instanceof Promise) {
        action.catch((e) => {
            if (e instanceof TimeoutError) {
                store.dispatch({ type: 'timeout_error', error: e })
            }
        })
    }
    next(action)
}

export default timeoutMiddleware
