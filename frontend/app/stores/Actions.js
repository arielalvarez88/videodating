const Actions = {
    REQUEST_VIDEO_RATING : 'request_video_rating',
    REQUEST_VIDEO_RATING_SUCCESS : 'request_video_rating_success',
    REQUEST_VIDEO_RATING_FAILURE : 'request_video_rating_failure',
    REQUEST_SET_GENDER : 'request_set_gender',
    REQUEST_SET_GENDER_SUCCESS : 'request_set_gender_success',
    REQUEST_SET_GENDER_FAILURE : 'request_set_gender_failure',
    POP_PROFILE_SUGGESTION: 'pop_profile_suggestion'
}



export default Actions
