// @flow
import { Alert } from 'react-native'
import type { SocialProviderName } from "../../utils/social/SocialProviderName";
import backendProxy from "app/utils/backend/BackendProxy";
import SocialProviderFactory from "../../utils/social/SocialProviderFactory";
import type { UserData } from "../../utils/social/UserData";
import type { Profile } from "../../models/Profile";
import { navigateMain } from "./NavigationActions";
import type { Settings } from "../../models/Settings";
import * as firebase from 'firebase';
import UserTypes from 'app/enums/UserTypes'
import DistanceUnits from 'app/enums/DistanceUnits'
import RNFetchBlob from 'react-native-fetch-blob'
import { mapUserDataToProfile } from 'app/utils/social/SocialUtils'
import { setLoading } from '../../stores/actionCreators/NotificationActions';
import { PermissionsAndroid } from 'react-native';
import { permissionGranted, permissionDenied } from 'app/stores/actionCreators/PermissionsActions';
import { hasLocationPermission, requestLocationPermission } from 'app/utils/PermissionsUtil';
import LocationUtils from "app/utils/LocationUtils";
import VideoRatings from "../../enums/VideoRatings";
import type { VideoRating } from "../../enums/VideoRatings";
import Actions from "../Actions";
import type { Gender } from 'app/enums/Genders'

const getFBProfile = async (): Promise<Profile> => {
    const provider = SocialProviderFactory.getInstance('facebook')
    const userData: UserData = await provider.getUserData()
    const profile: Profile = mapUserDataToProfile(provider.getProviderName(), userData)

    return profile
}


const login = (socialProvider: SocialProviderName, navigate: Function) => {

    return async (dispatch: Function) => {

        dispatch({
            type: "request_login",
            socialProvider: socialProvider
        })

        dispatch(setLoading(true, "Logging in"))

        let profile: ?Profile;

        switch (socialProvider) {
            case 'facebook':
                try {
                    profile = await getFBProfile()
                } catch (e) {
                    if (e.errorType === "OAuthException") {
                        console.debug('Token has expired, making user accept again')
                        dispatch({ type: 'request_login_retrying', reason: e })
                        dispatch(setLoading(false))
                        Alert.alert("Please login again", "For security reasons, Facebook needs you to relogin every 60 days. Please click the Facebook login button to login.")
                        return;
                    }
                    else {
                        dispatch({ type: 'request_login_failure', socialProvider: socialProvider, error: e })
                    }

                    return;
                }

                break
            default:
                profile = await getFBProfile()
        }

        try {
            const firebaseUser: firebase.FirebaseUser = await backendProxy.login(profile)

            profile = await backendProxy.getProfile()

            dispatch({ type: 'request_login_success', socialProvider, firebaseUser, profile })
            dispatch(refreshLocation())
            dispatch(navigateMain(navigate))
            dispatch(setLoading(false))
        } catch (e) {
            dispatch({ type: 'request_login_failure', socialProvider: socialProvider, error: e })
        }

    }
}

const updateSettings = (vals: Object) => {

    return async (dispatch: Function) => {
        dispatch({ type: 'request_update_settings', ...vals })
        try {
            await backendProxy.updateSettings(vals)
            dispatch({ type: 'request_update_settings_success', ...vals })
            dispatch(setFirstLoginFalse())
        } catch (e) {
            dispatch({ type: 'request_update_settings_failure', ...vals })
        }



    }


}


const setFirstLoginFalse = () => {

    return async (dispatch: Function) => {
        let profile: ?Profile


        try {
            profile = await backendProxy.getProfile()
            dispatch({ type: 'request_update_profile', profile: profile, update: { firstLogin: false } })
            await backendProxy.updateProfile({ firstLogin: false })
            dispatch({ type: 'request_update_profile_success', profile: profile, update: { firstLogin: false } })

        } catch (e) {

            dispatch({ type: 'request_update_profile_failure', profile: profile, update: { firstLogin: false }, error: e })
        }



    }
}

const updateVideo = (uri: string) => {
    return async (dispatch: Function) => {

        dispatch(setLoading(true, "Uploading video"))
        dispatch({ type: 'request_upload_video', uri: uri })
        try {
            const videoInfo = await backendProxy.uploadVideo(uri)
            dispatch({ type: 'request_upload_video_success', videoInfo })

        } catch (e) {
            dispatch({ type: 'request_upload_video_failure', uri: uri, error: e })

        } finally {
            dispatch(setLoading(false))
        }


    }
}

/**
 * Get new set of matches
 * @return {Promise<Profile>}
 */
const getMatches = (profile: ?Profile): Function => {

    return async (dispatch: Function) => {
        dispatch({ type: "request_matches" })

        profile = profile || await backendProxy.getProfile()
        if (!profile) {
            return []
        }
        let hasLocationPermissions = await hasLocationPermission()

        if (!hasLocationPermissions) {

            hasLocationPermissions = await requestLocationPermission()
        }
        let permissionSetter = hasLocationPermissions ? permissionGranted : permissionDenied
        dispatch(permissionSetter(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION))

        await refreshLocation()(dispatch)

        let latLong: [number, number]
        const distanceUnit = profile.settings.distanceUnit

        const maxDistance = distanceUnit === DistanceUnits.KM ? profile.settings.maxDistance : LocationUtils.transformKmToMi(profile.settings.maxDistance)

        switch (profile.userType) {
            case UserTypes.NORMAL:
                latLong = await LocationUtils.getLatLong()
                break;
            case UserTypes.PREMIUM:

                if (profile.paidLocation) {
                    latLong = profile.paidLocation
                } else {
                    latLong = await LocationUtils.getLatLong()
                }
                break;
            default:
                latLong = await LocationUtils.getLatLong()

        }

        try {

            const matches = await backendProxy.getMatches({
                latLong: latLong,
                radius: maxDistance
            })
            dispatch({ type: 'request_matches_success', matches: matches })
        } catch (e) {
            dispatch({ type: 'request_matches_failure' })
        }

    }
}

const refreshLocation = (): Function => {

    return async (dispatch) => {

        let coordinates: Position
        try {
            dispatch({ type: "fetching_location" })
            coordinates = await LocationUtils.getLocation()
            dispatch({ type: "fetching_location_success" })
        } catch (error) {
            dispatch({ type: "fetching_location_error", error })
            return
        }

        try {
            dispatch({ type: "request_update_location" })
            await backendProxy.updateLocation(coordinates)
            dispatch({ type: "request_update_location_success", coordinates })

        } catch (error) {
            dispatch({ type: "request_update_location_failure", error })
        }

    }
}


/**
 * @typedef {Object} RateIntent
 * @property {string} uri - URI of the video to rate from user
 * @property {VideoRating} rate - The rate given to the project
 * @property {string} uri - URI of the video to rate from user
 * 
 */
type RateIntent = {
    uri: string,
    rate: VideoRating,
    userId: string,
    videoId: string
}

const reactToVideo = (rateIntent: RateIntent) => {
    return async (dispatch: Function) => {
        try {
            dispatch({ type: Actions.REQUEST_VIDEO_RATING, rateIntent })
            await backendProxy.reactToVideo(rateIntent)
            dispatch({ type: Actions.REQUEST_VIDEO_RATING_SUCCESS, rateIntent })

        } catch (error) {
            dispatch({ type: Actions.REQUEST_VIDEO_RATING_FAILURE, rateIntent, error })

        }
    }
}

const setGender = (gender: Gender) => {
    return async (dispatch: Function) => {
        dispatch({ type: Actions.REQUEST_SET_GENDER, gender })
        try {
            await backendProxy.setGender(gender)
            dispatch({ type: Actions.REQUEST_SET_GENDER_SUCCESS, gender })
        } catch (error) {

            dispatch({ type: Actions.REQUEST_SET_GENDER_FAILURE, error, gender })
        }

    }
}

const popProfileSuggestion = () => {
    return (dispatch: Function) => {
        dispatch({ type: Actions.POP_PROFILE_SUGGESTION })
    }
}
export { login, updateSettings, setFirstLoginFalse, updateVideo, getMatches, reactToVideo, setGender, popProfileSuggestion } 
export type { RateIntent }