//@flow



const setLoading = (loading: boolean, message?: string = "Please wait") => {
    if (typeof message === 'undefined') {
        message = "Please wait"
    }
    return (dispatch: Function) => {

        dispatch({ type: "set_loading", loading, message })
    }

}

export { setLoading }