// @flow

const permissionDenied = (permissionName: string): Function => {

    return (dispatch: Function) => {
        dispatch({ type: 'permission_denied', permissionName: permissionName })
    }
}


const permissionGranted = (permissionName: string): Function => {
    return (dispatch: Function) => {
        dispatch({ type: 'permission_granted', permissionName: permissionName })
    }
}


export { permissionDenied, permissionGranted, } 