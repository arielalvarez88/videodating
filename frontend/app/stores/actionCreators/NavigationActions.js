// @flow

import backendProxy from 'app/utils/backend/BackendProxy'
import TimeoutError from '../../exceptions/TimeoutError';
import type { Settings } from '../../models/Settings';
import type { Profile } from '../../models/Profile';
import { setFirstLoginFalse } from './BackendActions';




const navigateMain = (navigate: Function) => {

    return async (dispatch: Function) => {
        try{
            dispatch({ type: 'request_get_profile' })
            const profile: ?Profile = await backendProxy.getProfile()
            dispatch({ type: 'request_get_profile_successful', profile: profile })
            if(profile){
                dispatch({ type: 'navigate_main', firstLogin: Boolean(profile.firstLogin), dispatch, navigate })
            }else{
                dispatch({ type: 'navigate', section : 'login', reason: 'No profile created', navigate})    
            }
            
        }catch(e){
            dispatch({ type: 'request_get_profile_failure', error: e })
        }    
    }
}

const navigate = (section: string, navigate: Function, reason?: String) => {

    return async (dispatch: Function) => {
        try{
            if(section === 'rate' || section === 'profile'){
                dispatch(setFirstLoginFalse())
            }    
        }catch(e){            
            //if an error occures updating firstLogin prop, is no big deal.
            console.error(e)
        }
        
        dispatch({type: 'navigate', section, reason, navigate})
        
    }
    

}

export {navigateMain, navigate}