// @flow

export type Notifications = {

    chatMessages: boolean,
    videoCalls: boolean,
}
