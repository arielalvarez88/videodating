// @flow
import { defaultVideo } from 'app/models/VideoInfo'
import type { VideoInfo } from 'app/models/VideoInfo'
import type { Settings } from './Settings'
import type { Location } from './Location'
import type {Gender} from 'app/enums/Genders'
import { defaultSettings } from './Settings'
import { defaultLocation } from './Location'
import UserTypes from 'app/enums/UserTypes'
import type { UserType } from 'app/enums/UserTypes'


export type Profile = {
    id?: string,
    displayName: string,
    firstName: string,
    firstLogin: boolean,
    middleName?: string,
    lastName: string,
    email: string,
    birthday: string,
    nickName: ?string,
    facebookId: ?string,
    principalVideo: VideoInfo,
    videos: VideoInfo[],
    gender: Gender | '',
    description: string,
    hobbies: ?string[],
    music: ?string[],
    token: ?string,
    settings: Settings,
    userType: UserType,
    paidLocation: ?[number, number],
    /**
     * Rates you have given to videos.
     * They keys are the userId, and values another object.
     * The values: The values' keys are the URI of the video and the values are rating (of type VideoRating). e.g.
     * {
     *  'userId1' : {
     *      'uri1' : 'good'
     *  }
     * }
     */
    reactionsToVideos: Object

};


const defaultProfile: Profile = {
    displayName: '',
    firstLogin: true,
    firstName: '',
    middleName: '',
    lastName: '',
    email: '',
    birthday: '',
    gender: '',
    description: '',
    hobbies: null,
    music: null,
    token: null,
    paidLocation: null,
    settings: defaultSettings,
    nickName: '',
    facebookId: '',
    principalVideo: defaultVideo,
    userType: UserTypes.NORMAL,
    videos: [],
    reactionsToVideos: {}



}
export { defaultProfile }

