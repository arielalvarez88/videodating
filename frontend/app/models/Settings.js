// @flow
import type {Notifications} from './Notifications'

export type Settings = {

    maxDistance: number,
    minAge: number,
    maxAge: number,
    distanceUnit: string,
    showMeMen: boolean,
    showMeWomen: boolean,
    notifications : Notifications
};

const defaultSettings: Settings = {    
    maxDistance: 30,
    minAge: 18,
    maxAge: 40,
    distanceUnit: 'km',
    showMeMen: true,
    showMeWomen: true,
    notifications : {
        chatMessages: true,
        videoCalls: true
    }
}

export { defaultSettings }