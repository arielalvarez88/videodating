import { timestamp } from "rxjs/operators";

// @flow


export type Location = {
    latitude: ?Number,
    longitude: ?Number,
    altitude: ?Number,
    actualCountry: ?String,
    configuredCountry: ?String,
    actualCity: ?String,
    configuredCity: ?String,
    timestamp: ?String
};

const defaultLocation: Location = {
    latitude: null,
    longitude: null,
    altitude: null,
    actualCountry: null,
    configuredCountry: null,
    actualCity: null,
    configuredCity: null,
    timestamp: null
}

export { defaultLocation }
