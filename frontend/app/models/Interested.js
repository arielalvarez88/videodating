// @flow

export type Interested = {
    men: number,
    women: number
};
