export type VideoInfo = {
    id: string,
    uri: string,
    iuck: number,
    bad: number,
    good: number,
    cool: number,
    awesome: number
}

const defaultVideo = {
    id: '',
    uri: '',
    iuck: 0,
    bad: 0,
    good: 0,
    cool: 0,
    awesome: 0
}

export { defaultVideo }