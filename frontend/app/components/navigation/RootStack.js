import Login from '../login/Login';
import { StackNavigator } from 'react-navigation';
import Settings from '../settings/Settings'
import Rate from '../rate/Rate'
import Summary from '../summary/Summary'
import Profile from '../profile/Profile'

const AppStack = StackNavigator(
    {
        Login: {
            screen: Login,
        },
        Settings: {
            screen: Settings,
        },
        Rate: {
            screen: Rate,
        },
        Summary: {
            screen: Summary,
        },
        Profile: {
            screen: Profile,
        }
    },
    {
        initialRouteName: 'Login',
    },
);

/*const AuthStack = createSwitchNavigator({
    Login: Login     
})

const switchStack = createSwitchNavigator({
    AuthLoading: Login,
    App: AppStack,
    Auth: AuthStack
},
{
    initialRouteName: 'AuthLoading',
})*/

export default AppStack;
