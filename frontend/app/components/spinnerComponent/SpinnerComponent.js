/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


import React, { Component } from 'react';
import { Text } from 'react-native';
import { connect } from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';

type Props = {
    loading: boolean;
    loadingMessage: String;
    children: any;
};
class SpinnerComponent extends Component<Props> {

    render() {
        const { loading, loadingMessage } = this.props

        return (
            <Spinner color={"#FFFFFF"} visible={loading} textContent={loadingMessage || 'Please wait'} />
        )
    }
}


const mapStateToProps = (state) => {
    return {
        loading: state.Notification.loading,
        loadingMessage: state.Notification.loadingMessage
    }
}
export default connect(mapStateToProps, null)(SpinnerComponent)
