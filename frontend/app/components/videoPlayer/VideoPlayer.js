'use strict';

import React, {
  Component
} from 'react';
import ReactNativeVideoPlayer from 'react-native-video-player';


type Props = {

}
class VideoPlayer extends ReactNativeVideoPlayer {
  constructor(props){
    super(props)
  }
  play = ()=>{
    return this.onStartPress()
  }
};

export default VideoPlayer