/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { LoginButton, AccessToken } from 'react-native-fbsdk';
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { connect } from 'react-redux';
import { StackNavigator } from 'react-navigation';
import TimeoutError from '../../exceptions/TimeoutError'
import SocialProviderFactory from '../../utils/social/SocialProviderFactory'
import type { Profile } from '../../models/Profile';
import UserData from '../../utils/social/UserData';
import { SocialProvider } from '../../utils/social/SocialProvider';
import { navigateMain } from '../../stores/actionCreators/NavigationActions';
import backendProxy from 'app/utils/backend/BackendProxy';
import { login } from '../../stores/actionCreators/BackendActions';
import { setLoading } from '../../stores/actionCreators/NotificationActions';

import { PermissionsAndroid } from 'react-native';


type Props = {
    navigation: Object;
    dispatch: Function;
};
class Login extends Component<Props> {
    static navigationOptions = {
        headerTitle: 'Login'
    };

    provider: SocialProvider;

    async componentWillMount() {

        this.props.dispatch(setLoading(true, "Logging in"))
        const loggedToken: any = await AccessToken.getCurrentAccessToken()
        if (loggedToken !== null) {
            this.props.dispatch(login('facebook', this.props.navigation.navigate))
        }else{
            this.props.dispatch(setLoading(false))
        }


    }


    render(): any {
        const { dispatch } = this.props;
        return (
            <View>

                <LoginButton
                    onLoginFinished={
                        async (error: Error, result: any) : any => {
                            if (error) {
                                console.error(error)
                                return;
                            }

                            dispatch(login('facebook', this.props.navigation.navigate))
                        }
                    }
                    onLogoutFinished={() => { }}
                    readPermissions={['email', 'user_birthday', 'user_friends']}
                />
            </View>
        )
    }
}


const mapDispatchToProps = (dispatch: Function): Object => ({ dispatch })
export default connect(null, mapDispatchToProps)(Login) 
