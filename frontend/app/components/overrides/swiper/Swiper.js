import Swiper from 'react-native-deck-swiper'
import { PanResponder, Animated} from 'react-native'

class SwiperCmp extends Swiper {
    constructor(props){
        super(props)
        //this.initDeck()
    }
    /*componentWillReceiveProps = () => {
        //Empty ovewrite on purpose
    }
    componentWillMount = () => {
        //Empty ovewrite on purpose
    }*/
    /**
     * @override
     */
    calculateCardIndexes = (firstCardIndex, cards) => {
        firstCardIndex = firstCardIndex || 0
        const previousCardIndex = firstCardIndex === 0 ? cards.length - 1 : firstCardIndex - 1
        const secondCardIndex = firstCardIndex === cards.length - 1 ? 0 : firstCardIndex + 1
        return { firstCardIndex: 0, secondCardIndex: 1, previousCardIndex: 0 }
      }
    
  
    /*initDeck = () =>{
        this._animatedValueX = 0
        this._animatedValueY = 0
    
        this.state.pan.x.addListener(value => (this._animatedValueX = value.value))
        this.state.pan.y.addListener(value => (this._animatedValueY = value.value))
    
        this.initializeCardStyle()
        this.initializePanResponder()
    }

    static getDerivedStateFromProps = (newProps) => {

        const calculateCardIndexes = (firstCardIndex, cards) => {
            firstCardIndex = firstCardIndex || 0
            const previousCardIndex = firstCardIndex === 0 ? cards.length - 1 : firstCardIndex - 1
            const secondCardIndex = firstCardIndex === cards.length - 1 ? 0 : firstCardIndex + 1
            return { firstCardIndex, secondCardIndex, previousCardIndex }
        }

        const rebuildStackAnimatedValues = (cards, cardIndex) => {
            const stackPositionsAndScales = {}

            cards.slice(cardIndex).forEach((card, index) => {
                const dIndex = cardIndex + index
                const factor = index < newProps.stackSize ? index : newProps.stackSize
                stackPositionsAndScales[`stackPosition${dIndex}`] = new Animated.Value(newProps.stackSeparation * factor)
                stackPositionsAndScales[`stackScale${dIndex}`] = new Animated.Value((100 - newProps.stackScale * factor) * 0.01)
            })

            return stackPositionsAndScales
        }

        return {
            ...calculateCardIndexes(newProps.cardIndex, newProps.cards),
            cards: newProps.cards,
            previousCardX: new Animated.Value(newProps.previousCardInitialPositionX),
            previousCardY: new Animated.Value(newProps.previousCardInitialPositionY),
            swipedAllCards: false,
            panResponderLocked: newProps.cards && newProps.cards.length === 0,
            slideGesture: false,
            ...rebuildStackAnimatedValues(newProps.cards, newProps.cardIndex)
        }
    }*/
}

export default SwiperCmp
