// @flow
import React, { Component } from 'react';
import NavigationBar from '../navigationBar/NavigationBar'


type NavigationBarViewProps = {
    navigation: Object;
}


class NavigationBarView<T : NavigationBarViewProps, S= {}> extends Component<T, S> {
    static navigationOptions = ({ navigation } : {navigation: any}) => {
        const params = navigation.state.params || {
            getHeaderTitle: () => ('')
        };
        
        return {
            headerTitle: params.getHeaderTitle(),
            header: (
                <NavigationBar navigation={navigation} />
            ),
        };
    };

    constructor(props: T) {
        super(props)
        props.navigation.setParams({
            getHeaderTitle: this.getHeaderTitle
        })
    }

    /**
     * Override this in children to provide the title of the section
     * @override
     */
    getHeaderTitle() {
        return ''
    }
}
export default NavigationBarView
export type { NavigationBarViewProps } 