/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


import React, { Component } from 'react'
import {
    View,
    Text
} from 'react-native'

import { connect } from 'react-redux'
import Button from '../../components/button/Button'
import { updateSettings } from '../../stores/actionCreators/BackendActions'
import { navigate } from '../../stores/actionCreators/NavigationActions'
import { ButtonStyles } from '../../styles/Button'

type Props = {
    navigateSettings: Function,
    navigateRate: Function,
    navigateProfile: Function,
    settingsButtonStyle: Object,
    rateButtonStyle: Object,
    profileButtonStyle: Object

};

class NavigationBarCmp extends Component<Props> {


    render(): any {
        const { navigateSettings, navigateRate, navigateProfile, settingsButtonStyle, rateButtonStyle, profileButtonStyle } = this.props
        
        return (
            <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
                <Button style={{...settingsButtonStyle, flex: 1, height: 40}} title="Config" onPress={navigateSettings} />
                <Button style={{...rateButtonStyle , flex: 1, height: 40}} title="Rate" onPress={navigateRate} />
                <Button style={{...profileButtonStyle, flex: 1, height: 40}} title="Contacts" onPress={navigateProfile} />
            </View>
        )
    }

}
const mapStateToProps = (state) => {
    const settingsButtonStyle = state.Navigation === 'Summary' ? ButtonStyles.selected : ButtonStyles.unselected
    const rateButtonStyle = state.Navigation === 'Rate' ? ButtonStyles.selected : ButtonStyles.unselected
    const profileButtonStyle = state.Navigation === 'Contacts' ? ButtonStyles.selected : ButtonStyles.unselected
    return {
        settingsButtonStyle,
        rateButtonStyle,
        profileButtonStyle
    }

}
const mapDispatchToProps = (dispatch, props) => {
    return ({
        navigateSettings: () => {
            dispatch(navigate("Summary", props.navigation.navigate))
        },
        navigateRate: () => {
            dispatch(navigate("Rate", props.navigation.navigate))
        },
        navigateProfile: () => {
            dispatch(navigate("Contacts", props.navigation.navigate))
        }
    })
}

const NavigationBar = connect(mapStateToProps, mapDispatchToProps)(NavigationBarCmp)
export default NavigationBar

