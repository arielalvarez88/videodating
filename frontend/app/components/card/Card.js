// @flow

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import VideoPlayer from 'react-native-video-player';
import Video from 'react-native-video';
import { connect } from 'react-redux';
import { calculateAge } from 'app/utils/social/SocialUtils';
import VideoReactions from 'app/components/videoReactions/VideoReactions'
import FontSyles from 'app/styles/Font'
import type { Profile } from 'app/models/Profile';
import { Maybe, compose, map } from '../../utils/functionalProgramming/F'



type Props = {
    profile: Profile
};

const styles = {
    cardContainer: {
        padding: 10,
        backgroundColor: "white"
    }
}
type State = {
    isPlaying: boolean;
}
export default class Card extends Component<Props, State>{

    videoPlayer: ?VideoPlayer
    state = {
        isPlaying: false
    }
    setVideoPlayer = (videoPlayer: ?VideoPlayer) => {
        
        if(this.videoPlayer && !videoPlayer){
            this.videoPlayer.stop()
        }else if(videoPlayer){
            videoPlayer.onStartPress()
        }
        this.videoPlayer = videoPlayer
        
    }

    render() {
        const { profile } = this.props
        const video = profile.principalVideo && profile.principalVideo.uri? 
        /*<Video
          source={{ uri:  profile.principalVideo  }}
          ref={this.setVideoPlayer}
          playInBackground={true}
          playWhenInactive={true}
          paused={!this.state.isPlaying}
          style={
            { width: 500, height: 500 }
          }
          rate={1.0}                              // 0 is paused, 1 is normal.
          volume={1.0}                            // 0 is muted, 1 is normal.
          onBuffer={(status) => {
           
            console.log("Buffering : ", status);
          }}
          onLoadStart={(...args)=>{
            console.log("onLoadStart: ", args)
          }}            // Callback when video starts to load
          onLoad={(...args)=>{
            console.log("onLoad: ", args)
          }}               // Callback when video loads
          onProgress={(...args)=>{
            //console.log("onProgress: ", args)
          }}               // Callback every ~250ms with currentTime
          onEnd={()=>{
            console.log("onEnd")
          }}                      // Callback when playback finishes
          
          onError={(...args)=>{
            console.log("onError: ", args)
          }}               // Callback when video cannot be loaded          
          onTimedMetadata={(...args)=>{
            console.log("onTimedMetadata: ", args)
          }}  // Callback when the stream receive some metadata

          muted={false}                           // Mutes the audio entirely.
          resizeMode="cover"                      // Fill the whole screen at aspect ratio.*
          repeat={true}                           // Repeat forever.
          playInBackground={false}                // Audio continues to play when app entering background.
          playWhenInactive={false}                // [iOS] Video continues to play when control or notification center are shown.

        />*/

        <VideoPlayer
            video={profile.principalVideo}
            controlsTimeout={5000}
            ref={this.setVideoPlayer}
            autoplay={false}
            videoWidth={500}
            videoHeight={500}
            playInBackground={false}
            playWhenInactive={false}
            paused={true}
            onBuffer={this.onBuffer}
            rate={1.0}                              // 0 is paused, 1 is normal.
            volume={1.0}                            // 0 is muted, 1 is normal.
            muted={false}                           // Mutes the audio entirely.
            resizeMode="cover"                      // Fill the whole screen at aspect ratio.*
            repeat={false}                           // Repeat forever.



        /> : <Text> No video </Text>

        return (
            <View style={styles.cardContainer}>
                {video}
                <View>
                    
                    <Text style={FontSyles.bigFont}>
                        {profile.displayName}, {calculateAge(profile.birthday)}
                    </Text>

                </View>


                <VideoReactions userId={profile.id} uri={profile.principalVideo.uri} videoId={profile.principalVideo.id}/>

            </View>
        )
    }

    onBuffer = (status : {isLoading : boolean}) => {
        console.log("buffering", status);
     
        
    }

    play = () => {
        this.setState({ isPlaying: true })
        Maybe.of(this.videoPlayer).map(player => player.onStartPress())
    }
    stopVideo = () => {
        this.setState({ isPlaying: false })
        Maybe.of(this.videoPlayer).map(player => player.stop())

    }
    isPlaying = (): boolean => {
        return this.state.isPlaying
    }

}