//@flow

import React, { Component } from 'react'
import { Text, View, TouchableHighlight } from 'react-native'
import nodeEmoji from 'node-emoji';
import Styles from './VideoReactionsStyle'
import VideoRatings from 'app/enums/VideoRatings'
import { connect } from 'react-redux';
import type { Profile } from 'app/models/Profile'
import { path, safePath } from 'app/utils/functionalProgramming/F'
import type { RateIntent } from 'app/stores/actionCreators/BackendActions'
import { reactToVideo } from 'app/stores/actionCreators/BackendActions'
type Props = {
    dispatchReactToVideo: Function,
    userId: String,
    uri: String,
    videoId: String,
    /**
    * Rates you have given to videos.
    * They keys are the userId, and values another object.
    * The values: The values' keys are the URI of the video and the values are rating (of type VideoRating). e.g.
    * {
    *  'userId1' : {
    *      'uri1' : 'good'
    *  }
    * }
    */
    myPreviousReactions: Object
}

type State = {
    styles: {
        iuck: Object,
        bad: Object,
        good: Object,
        cool: Object,
        awesome: Object,
        text: Object
    }
}

const initialState = {
    styles: {
        iuck: Styles.rateIcon,
        bad: Styles.rateIcon,
        good: Styles.rateIcon,
        cool: Styles.rateIcon,
        awesome: Styles.rateIcon,
        text: {
            fontSize: 30
        }
    }

}
export class VideoReactions extends Component<Props, State>{
    state: State = initialState

    vote = (value: string) => {

        this.props.dispatchReactToVideo({
            userId: this.props.userId,
            uri: this.props.uri,
            rate: value,
            videoId: this.props.videoId
        })
        this.setState(prevState => {

            const returnVal = {
                styles: {
                    ...initialState.styles,
                    [value]: Styles.pressedRateIcon
                }
            };

            return returnVal

        })
    }


    static getDerivedStateFromProps(props: Props, state: State) {     

        return VideoReactions.getStateToHighlightPreviousReaction(props, state)

    }

    /**
     * If you have reacted to this video before, highlight your previous reaction.
     *      
     */
    static getStateToHighlightPreviousReaction(props: Props, state: State): Object {
        
        var updatesToState = {}
        safePath(['myPreviousReactions', props.userId, props.videoId], props).map((rateToThisVideo) => {

            updatesToState.styles = {
                ...initialState.styles,
                [rateToThisVideo]: Styles.pressedRateIcon
            }
        })

        return updatesToState
    }



    render() {
        const awesomeIcon = nodeEmoji.get("heart_eyes"),
            coolIcon = nodeEmoji.get("hushed"),
            goodIcon = nodeEmoji.get("thumbsup"),
            badIcon = nodeEmoji.get("expressionless"),
            iuckIcon = nodeEmoji.get("tired_face")



        return (

            

                <View flexDirection="row" justifyContent="space-between">

                    <TouchableHighlight onPress={() => {
                        this.vote(VideoRatings.IUCK)
                    }}
                        testID={VideoRatings.IUCK}
                        style={this.state.styles.iuck}
                    >
                    
                        <Text style={this.state.styles.text}>{iuckIcon}</Text>
                    
                        
                    </TouchableHighlight>


                    <TouchableHighlight onPress={() => {
                        this.vote(VideoRatings.BAD)
                    }}
                        testID={VideoRatings.BAD}
                        style={this.state.styles.bad}
                    >
                    
                        <Text style={this.state.styles.text}>{badIcon}</Text>
                    
                    </TouchableHighlight>

                    <TouchableHighlight onPress={() => {
                        this.vote(VideoRatings.GOOD)
                    }}
                        testID={VideoRatings.GOOD}
                        style={this.state.styles.good}
                    >
                    
                        <Text style={this.state.styles.text}>{goodIcon}</Text>
                    
                    </TouchableHighlight>



                    <TouchableHighlight onPress={() => {
                        this.vote(VideoRatings.COOL)
                    }}
                        testID={VideoRatings.COOL}
                        style={this.state.styles.cool}
                    >
                    
                        <Text style={this.state.styles.text}>{coolIcon}</Text>
                    
                    </TouchableHighlight>


                    <TouchableHighlight onPress={() => {
                        this.vote(VideoRatings.AWESOME)
                    }}
                        testID={VideoRatings.AWESOME}
                        style={this.state.styles.awesome}
                    >
                    
                        <Text style={this.state.styles.text}>{awesomeIcon}</Text>
                    
                    </TouchableHighlight>

                </View>
            
        )
    }
}

const mapStateToProps = (state, props) => {
    return {
        myPreviousReactions: state.Backend.profile.reactionsToVideos
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        dispatchReactToVideo: (rate: RateIntent) => {
            dispatch(reactToVideo(rate))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(VideoReactions)