/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


import React, { Component } from 'react'
import {
    Text,
    View,
    Slider,
    Switch,
    ScrollView,
    Button as ReactButton
} from 'react-native'
import { connect } from 'react-redux'
import Button from '../button/Button'
import { updateSettings } from '../../stores/actionCreators/BackendActions'
import { navigate } from '../../stores/actionCreators/NavigationActions'
import Gutter from '../../components/gutter/Gutter'
import type { Settings as UserSettings } from '../../models/Settings'
import { ButtonStyles } from '../../styles/Button'
import NavigationBarView from '../../components/navigationBarView/NavigationBarView'
import type { NavigationBarViewProps } from '../../components/navigationBarView/NavigationBarView'

type Props = NavigationBarViewProps & UserSettings & {
    update: Function,
    screenProps: ?Object,
    startMatching: Function

};

let style = {
    field: {
        marginTop: 10,
        marginBottom: 10
    },
    header: {
        marginBottom: 10
    },
    kmButton: {},
    miButton: {}
}

class SettingsComp extends NavigationBarView<Props> {

    slider: Slider

    prepareStyle(props) {
        const { distanceUnit, maxDistance, showMeWomen, showMeMen, update, minAge, maxAge } = this.props
        style.kmButton = distanceUnit === 'km' ? ButtonStyles.selected : ButtonStyles.unselected
        style.miButton = distanceUnit === 'mi' ? ButtonStyles.selected : ButtonStyles.unselected


    }

    render(): any {

        const { distanceUnit, maxDistance, showMeWomen, showMeMen, update, minAge, maxAge, notifications, startMatching } = this.props
        this.prepareStyle(this.props)
        return (
            <Gutter>
                <ScrollView contentContainerStyle={{ alignItems: 'stretch', justifyContent: 'center' }}>

                    <View style={style.field}>
                        <Text style={style.header}>Max distance: {maxDistance} {distanceUnit} </Text>
                        <Slider
                            value={10}
                            minimumValue={1}
                            maximumValue={100}
                            step={1}
                            onSlidingComplete={value => { update({ 'maxDistance': value }) }}
                        />


                    </View>


                    <View style={style.field}>
                        <Text style={style.header}>Distance Unit: {distanceUnit}</Text>

                        <View style={{ height: 40, flexDirection: 'row', justifyContent: 'space-around' }}>
                            <Button flex="1" style={{ flex: 1, height: 40, ...style.kmButton }} title="km" onPress={() => { update({ distanceUnit: 'km' }) }} />

                            <Button style={{ flex: 1, height: 40, ...style.miButton }} title="mi" onPress={() => { update({ distanceUnit: 'mi' }) }} />
                        </View>

                    </View>


                    <View style={style.field}>
                        <Text style={style.header}>Show me:</Text>

                        <View style={{ flexDirection: 'row' }}>
                            <Text >Men:</Text>
                            <Switch
                                value={showMeMen}
                                onValueChange={value => { update({ 'showMeMen': value }) }}
                            />

                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <Text>Women:</Text>
                            <Switch
                                value={showMeWomen}
                                onValueChange={value => { update({ 'showMeWomen': value }) }}
                            />
                        </View>
                    </View>

                    <View style={style.field}>
                        <Text style={style.header}>Age :</Text>
                        <Text> Min Age: {minAge}</Text>
                        <Slider
                            value={18}
                            minimumValue={18}
                            maximumValue={100}
                            step={1}
                            onSlidingComplete={value => { update({ 'minAge': value }) }}
                        />
                        <Text> Max Age: {maxAge}</Text>
                        <Slider
                            value={50}
                            minimumValue={18}
                            maximumValue={100}
                            step={1}
                            onSlidingComplete={value => { update({ 'maxAge': value }) }}
                        />


                    </View>

                    <View style={style.field}>
                        <Text style={style.header}>Notifications :</Text>

                        <View style={{ flexDirection: 'row' }}>
                            <Text>Chat Messages:</Text>
                            <Switch
                                value={notifications.chatMessages}
                                onValueChange={value => { update({ 'notifications': { ...notifications, chatMessages: value } }) }}
                            />
                        </View>


                    </View>


                    <View style={style.field}>
                        <ReactButton title="Start matching with people" onPress={startMatching} />


                    </View>





                </ScrollView>
            </Gutter>
        )
    }

}
const stateToProps = (state) => {

    const settings: UserSettings = state.Backend.profile.settings
    const mapping = {
        ...settings
    }
    return mapping
}

const dispatchToProps = (dispatch, props) => {
    return {
        update: (vals) => {
            dispatch(updateSettings(vals))
        },
        startMatching: () => {
            dispatch(navigate('Rate', props.navigation.navigate))
        }
    }
}

const Settings = connect(stateToProps, dispatchToProps)(SettingsComp)
export default Settings
