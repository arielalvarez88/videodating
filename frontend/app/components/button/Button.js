//@flow

import React, { Component } from 'react'



import { View, TouchableNativeFeedback, Text } from 'react-native'

type Props = {
    onPress: Function,
    style?: any,
    title: string
}

class Button extends Component<Props>{



    render() {
        const style: any = Object.assign({ height: 10 }, this.props.style || {})
        return (
            <TouchableNativeFeedback
                onPress={this.props.onPress}

            >

                <View style={{ height: style.height }} flex={1}  >
                    <Text style={
                        {
                            textAlign: 'center',
                            textAlignVertical: 'center',
                            ...style
                        }
                    } >
                        {this.props.title}
                    </Text>
                </View>

            </TouchableNativeFeedback>
        )
    }
}

export default Button