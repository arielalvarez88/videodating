//@flow
/**
 * Providers a padding from the screen edge to the content.
 */
import React, { Component } from 'react'
import { View, TouchableNativeFeedback, Text } from 'react-native'


type Props = {
    onPress: Function,
    style?: Object,
    title: string
} & any

export default class Gutter extends Component<any>{

    render() {
        const style = "style" in this.props ? this.props.style : {}
        const props = { "style": {}, "children": {} }
        Object.assign(props, this.props)
        delete props.style
        delete props.children


        return (
            <View
                style={
                    {
                        padding: 10,
                        ...style
                    }
                }
                {...props}
            >
                {this.props.children}
            </View>
        )
    }
}