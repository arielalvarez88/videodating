//@flow
import React from 'react'
import { View, Button, StyleSheet, Text, Modal} from 'react-native'
import Genders from 'app/enums/Genders'
import type { Gender } from 'app/enums/Genders'
import { connect } from 'react-redux';
import { setGender } from '../../stores/actionCreators/BackendActions';
import GenderSelection from "app/components/genderSelection/GenderSelection";

import {path} from 'app/utils/functionalProgramming/F';

type Props = {
    isVisible: Function
}
type State = {

}
const Styles = StyleSheet.create({
    header: {
        fontWeight: "bold"
    }
})

export class GenderSelectionOverlay extends React.Component<Props, State>{

    render() {

        const { isVisible } = this.props

        return (
            <Modal 
            animationType="slide"
            transparent={false}
            visible={isVisible}
            onRequestClose={() => {
                
              }}
            >
                <GenderSelection />
            </Modal>
        )
    }
}

const mapStateToProps = (state: Object) => {
    return {
        isVisible: state.Backend.logged  && !Boolean(path(['Backend','profile','gender'],state))
    }
}
export default connect(mapStateToProps)(GenderSelectionOverlay)