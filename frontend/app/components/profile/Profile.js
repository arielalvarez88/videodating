/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


import React, { Component } from 'react';
import {
    Text,
    View,
    Button
} from 'react-native';
var ImagePicker = require('react-native-image-picker');
import { connect } from 'react-redux'
const Maybe = require('folktale').maybe
import VideoPlayer from 'react-native-video-player';
import NavigationBarView from '../../components/navigationBarView/NavigationBarView'
import type { NavigationBarViewProps } from '../../components/navigationBarView/NavigationBarView'

import {path} from 'app/utils/functionalProgramming/F'
import { updateVideo } from '../../stores/actionCreators/BackendActions'

type ProfileCmpProps = NavigationBarViewProps & {
    videoInBackend: Boolean;
    videoUri: ?string;
    uploadVideo: Function;


}

type ProfileState = {
    uri: ?string;
    newVideo: boolean
}

class ProfileCmp extends NavigationBarView<ProfileCmpProps, ProfileState> {

    state = {
        uri: null,
        newVideo: false
    }
    /**
     * Reference to remove the listener to the willBlur event of navigation
     */
    willBlurSubscription: ?{ remove: Function } = null

    options = {
        title: 'Select Video',
        mediaType: 'video',
        videoQuality: 'high',
        takePhotoButtonTitle: 'Take Video Now',
        durationLimit: 300,
        customButtons: [
            { name: 'fb', title: 'Choose Photo from Facebook' },
        ],
        storageOptions: {
            skipBackup: true,
            path: 'images'
        }
    }
    videoPlayer: VideoPlayer = null

    constructor(props) {
        super(props)
    }


    componentWillMount = () => {
        this.willBlurSubscription = this.props.navigation.addListener("willBlur", payload => {

            if (this.videoPlayer) {
                this.videoPlayer.stop()
            }

        })

    }

    componentWillUnmount = () => {
        Maybe.of(this.willBlurSubscription).map(willBlurSubscription => willBlurSubscription.remove())
    }

    uploadVideo = () => {
        try {
            this.props.uploadVideo(this.state.uri)
        } catch (e) {
            console.error(e);
        }
    }

    selectVideo = () => {
        ImagePicker.showImagePicker(this.options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.setState({
                    uri: response.uri,
                    newVideo: true
                })
                //this.props.updateVideo(response.uri)
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                console.log("Source: ", source)
            }
        });


    }
    render = (): any => {
        const { videoInBackend, videoUri } = this.props
        const { newVideo, uri } = this.state
        this.videoPlayer = null
        const videoUriToPreview = newVideo ? this.state.uri : videoUri
        const video = videoInBackend || newVideo ?
            <VideoPlayer
                video={{ uri: videoUriToPreview }}
                controlsTimeout={5000}
                ref={(ref) => this.videoPlayer = ref}
                playInBackground={false}
                playWhenInactive={false}
                autoplay={true}
                videoWidth={500}
                videoHeight={500} />

            : <Text>You haven't uploaded a video</Text>
        const uploadButton = newVideo ? <Button title={"Upload Video"} onPress={this.uploadVideo} /> : null

        return (
            <View>
                <Text> Video: </Text>
                {video}
                <View style={{ marginBottom: 10 }}>
                    <Button title={"Select Video"} onPress={this.selectVideo} />
                </View>
                {uploadButton}


            </View>
        )
    }
}

const mapStateToProps = (state, props) => {
    var uri = path(['principalVideo', 'uri'], state.Backend.profile)
    return {
        videoInBackend: Boolean(uri),
        videoUri: uri        
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        uploadVideo: (uri: string) => {
            dispatch(updateVideo(uri))
        }

    }
}
const Profile = connect(mapStateToProps, mapDispatchToProps)(ProfileCmp)
export default Profile