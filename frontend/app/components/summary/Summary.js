/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


import React, { Component } from 'react';
import {
    Text,
    View,
    Button
} from 'react-native';

import NavigationBarView from '../../components/navigationBarView/NavigationBarView'
import type { NavigationBarViewProps } from '../../components/navigationBarView/NavigationBarView'
import { navigate } from '../../stores/actionCreators/NavigationActions'
import { connect } from 'react-redux'


type Props = NavigationBarViewProps & {

    navigateToSettings: Function,
    navigateToProfile: Function


};

class SummaryCmp extends NavigationBarView<Props> {

    render(): any {
        const style = {
            margin: 15
        }
        const { navigateToSettings, navigateToProfile } = this.props
        return (
            <View>
                <View style={style}>
                    <Button title={"Edit settings"} onPress={navigateToSettings} />
                </View>

                <View style={style}>
                    <Button title={"Edit profile"} onPress={navigateToProfile} />
                </View>
            </View>
        )
    }
}

const dispatchToProps = (dispatch, props) => {
    return {
        navigateToSettings: () => {
            dispatch(navigate('Settings', props.navigation.navigate))
        },
        navigateToProfile: () => {
            dispatch(navigate('Profile', props.navigation.navigate))
        }
    }
}
const Summary = connect(null, dispatchToProps)(SummaryCmp)
export default Summary