//@flow
import React from 'react'
import { View, Button, StyleSheet, Text } from 'react-native'
import Genders from 'app/enums/Genders'
import type { Gender } from 'app/enums/Genders'
import { connect } from 'react-redux';
import { setGender } from '../../stores/actionCreators/BackendActions';

type Props = {
    setGender: Function
}
type State = {

}
const Styles = StyleSheet.create({
    header: {
        fontWeight: "bold",
        fontSize: 20,
        marginBottom: 20
    },
    buttons : {
        marginTop: 20,
        marginBottom: 20
    }
})

export class GenderSelection extends React.Component<Props, State>{

    render() {

        const { setGender } = this.props

        return (
            <View>
                <Text style={Styles.header}> Please choose your gender (more genders comming soon):  </Text>
                
                <View style={Styles.buttons}>
                    <Button  title="Male" testID="male_btn" onPress={_ => {
                        setGender(Genders.MALE)
                    }} />
                </View>
                
                <View style={Styles.buttons}>
                    <Button style={Styles.buttons}  title="Female" testID="female_btn" onPress={_ => {
                        setGender(Genders.FEMALE)
                    }} />
                </View>
            </View>
        )
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        setGender: (gender: Gender) => {
            dispatch(setGender(gender))
        }
    }
}
export default connect(null, mapDispatchToProps)(GenderSelection)