/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


import React, { Component } from 'react';
import {
    Text,
    View,
    ActivityIndicator,
} from 'react-native';
import VideoPlayer from 'app/components/videoPlayer/VideoPlayer';
import ErrorBoundary from 'app/components/ErrorBoundary'
import NavigationBarView from '../../components/navigationBarView/NavigationBarView'
import Card from 'app/components/card/Card';
import { connect } from 'react-redux';
import Video from 'react-native-video'; // eslint-disable-line
import type { NavigationBarViewProps } from '../../components/navigationBarView/NavigationBarView'
import type { Profile } from 'app/models/Profile'
import Swiper from 'app/components/overrides/swiper/Swiper'

import { curry, Maybe, Left, Right, IO, head, compose, map } from '../../utils/functionalProgramming/F'
import { push } from '../../utils/functionalProgramming/Arrays'
import { getMatches, popProfileSuggestion } from 'app/stores/actionCreators/BackendActions'
import { setLoading } from '../../stores/actionCreators/NotificationActions';
import fakeProfiles from '../../../mockupData/fakeProfiles';
import { path } from 'app/utils/functionalProgramming/F'


type Props = NavigationBarViewProps & {
    profile: Profile,
    suggestions: Profile[],
    fetchingMatches: boolean,
    getNextMatches: Function,
    userInRateSection: boolean,
    showLoadingMessage: Function,
    removeLoadingMessage: Function,
    popFromMatchesStack : Function
};

class Rate extends NavigationBarView<Props> {

    constructor(props: Props) {
        super(props)
        this.saveRefToCard = this.saveRefToCard.bind(this)
    }

   

    cards: Card[] = []



    saveRefToCard = (card: ?Card) => {

        const pushNotNull = compose(map(push(this.cards)), Maybe.of)
        pushNotNull(card).map(x => x.unsafePerformIO())

    }



    stopOldPlayNew = (cardIndex) => {

        /*if (this.cards.length <= cardIndex) {
            return;
        }

        if (cardIndex >= 0 && this.cards[cardIndex]) {
            this.cards[cardIndex].stopVideo()

        }

        if (this.cards.length > (cardIndex + 1)) {
            if (this.cards[cardIndex + 1] && !this.cards[cardIndex + 1].isPlaying()) {
                this.cards[cardIndex + 1].play(); 
            }
        }

        */

        if (cardIndex >= 0) {
            //this.cards.splice(cardIndex, 1);
        }

    }


    onSwiped = (suggestions)=>{
        this.props.popFromMatchesStack()

        if(suggestions.length <= 1 && !this.props.fetchingMatches){
            //this.props.getNextMatches()
        }
        
    }

    render() {
        const { profile, suggestions, fetchingMatches, popFromMatchesStack,getNextMatches } = this.props
        if(!fetchingMatches && suggestions.length <= 0){
            getNextMatches()
        }
        const content = fetchingMatches == true || suggestions.length <= 0 ? <ActivityIndicator size="large" color="#0000ff" /> : <Swiper
            cards={suggestions}
            flex={1}
            renderCard={(profile) => {

                return (
                    <Card key={profile.id} profile={profile} />
                )
            }}
            
            onSwiped={()=>(this.onSwiped(suggestions))}            
            cardIndex={0}
            keyExtractor = {(profile:Profile)=>(profile.id)}
            backgroundColor={'#F6F4F2'}
            stackSize={1}>

        </Swiper>

        return (
            <View style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
            }} >

                {content}

            </View>
        )

    }
}

const mapStateToProps = (state, props) => {

    return {
        profile: state.Backend.profile,
        fetchingMatches: state.Backend.fetchingMatches,
        suggestions: state.Backend.lastSuggestions || [],
        userInRateSection: state.Navigation === 'Rate'

    }
}


const mapDispatchToProps = (dispatch, props) => {

    return {
        getNextMatches: () => {                        
                dispatch(getMatches())            
        },
        popFromMatchesStack : (suggestions: Profile[]) =>{
            
            dispatch(popProfileSuggestion())
           
        },
        showLoadingMessage: () => {
            dispatch(setLoading(true, 'Searching for suggestions.'))
        },
        removeLoadingMessage: () => {
            dispatch(setLoading(false))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Rate)